<?php
	get_header();

	//Locations sorted in inc/hooks.php
	$overview_bg['afs-locations-overview'] = get_field('overview_bg');

	//Get Locations
	$args = array(
		'post_type' => 'location',
		'order' => 'ASC',
		'orderby' => 'menu_order'
	);

	$locations = new WP_Query($args);

?>

<main id="main" role="main">

	<!-- Section: Header Large -->
	<?php include('components/header-skinny.php'); ?>

	<div class="afs-section component-graytitle">
		<div class="graytitle-text">
			<?php echo get_field('locations_header_text', 'option')?>
		</div>
	</div>

	<div class="afs-section afs-locations-overview">
		<?php echo responsive_backgrounds_skinny($overview_bg) ?>

		<div class="container">

			<?php
				foreach ($locations->posts as $key => $location):
					$location_show = get_field("show_on_locations_archive", $location->ID);

					if ($location_show) : ?>

						<div class="overview-item">
							<a class="scroll-to" href="#<?php echo $location->post_name; ?>"><?php the_field("location_shortname", $location->ID);?></a>
						</div>

			<?php
					endif;
				endforeach ?>
		</div>
	</div>

	<?php if ($locations->have_posts()): ?>
		<!-- Section: Locations Archive -->
		<section class="afs-section afs-locations">

			<div class="container">

				<?php
					while($locations->have_posts()) :
						$locations->the_post();

						$location_show = get_field("show_on_locations_archive");

						if(!$location_show) {
							continue;
						}

						$location_image_main = get_field("location_photo_main");
						$location_image_mapsm = get_field("location_photo_map_sm");

						$location_map_embed = get_field("location_map_embed");

						$location_address = format_address( get_the_ID() );
						$location_gmap_link = get_field("location_gmap_link");
						$location_phone = get_field("location_phone");

						$location_hours = get_field("location_hours");
				?>

					<div id="<?php echo $post->post_name; ?>" class="location location-preview">
						<h1 class="afs-title-lg">
							<a href="<?php the_permalink();?>"><?php the_title(); ?></a>
						</h1>

						<div class="location-info">

							<?php
								//Location Image
								if ($location_image_main):
									foreach ($location_image_main as $key => $image): ?>

										<div class="location-front">

											<?php
												if ($image['location_photo_main_link']):

													echo '<a href="' . $image['location_photo_main_link']['url'] . '"><img src="' . $image['location_photo_main_image']['url'] . '" alt="' . get_the_title() . ' - Store Front"></a>';

												else:
											?>

												<img src="<?php echo $image['location_photo_main_image']['url'] ?>" alt="<?php the_title(); ?> - Store Front">

											<?php endif ?>

										</div>
							<?php
									endforeach;
								endif;
							?>



							<div class="location-details">
								<?php if (($location_address && $location_gmap_link) || $location_phone): ?>
									<div class="location-contact storecontact-<?php echo get_the_ID(); ?>">

										<?php if ($location_address && $location_gmap_link): ?>
											<address class="location-address">
												<a href="<?php echo $location_gmap_link; ?>" target="_blank">
													<?php echo $location_address; ?>
												</a>
											</address>
										<?php endif ?>

										<?php if ($location_phone): ?>
											<a class="location-phone" href="tel:+1<?php echo $location_phone; ?>"><?php echo format_phone( $location_phone ); ?></a>
										<?php endif ?>

									</div>
								<?php endif ?>


								<?php if ($location_hours): ?>

								

									<div class="location-hours storehours-<?php echo get_the_ID(); ?>">
										<?php foreach ($location_hours as $key => $set):

											//Only display 2 sets of hours
											if($key > 1) continue;

										?>
											<div class="hours">
												<h3><?php echo $set['info']['label'] ?>:</h3>
												<ul>
													<?php foreach ($set['hours'] as $key => $day): ?>
														<li><?php echo $day['day'] . ': ' . $day['time']; ?></li>
													<?php endforeach ?>
												</ul>

											</div>

										<?php endforeach; ?>
									</div>
								<?php endif; ?>

								<div class="location-link">
									<a class="btn--square" href="<?php the_permalink();?>"><?php the_field('locations_archive_page_button_label');?></a>
								</div>
							</div>
						</div>


						<div class="location-map" style="background-image: url('<?php echo $location_image_mapsm['url'] ?>')">
							<?php
								if ($location_map_embed):

									echo $location_map_embed;

								else: ?>

									<img src="<?php echo $location_image_mapsm['url'] ?>" alt="Close up map of America's Floor Source's <?php the_title(); ?> location">

							<?php endif; ?>
						</div>
					</div>

				<?php endwhile; ?>

			</div>
		</section>
	<?php
		endif;
	?>

</main>

<?php

	get_footer();
?>
