<div class="paginate">
<?php 
if(function_exists('wp_paginate')) {
	wp_paginate();
}
elseif(function_exists('default_paginate')) {
	default_paginate();
}
?>
</div>