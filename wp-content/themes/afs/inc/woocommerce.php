<?php
	/*
	Add Woocommerce Theme Support
	*/
	add_action( 'after_setup_theme', 'woocommerce_support' );
	function woocommerce_support() {
		add_theme_support( 'woocommerce' );
	}

	include('woocommerce/wc-functions.php');
	include('woocommerce/overrides.php');
	include('woocommerce/myaccount.php');
	include('woocommerce/forms.php');
	include('woocommerce/hooks.php');
	include('woocommerce/waiting-for-pickup.php');
