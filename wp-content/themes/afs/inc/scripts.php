<?php
// Register styles and scripts for header.
add_action( 'wp_enqueue_scripts', 'theme_scripts', 1 );

function theme_scripts() {

	wp_register_style( 'bootstrap-core-css', get_template_directory_uri() . '/assets/css/bootstrap.css');
	wp_enqueue_style( 'bootstrap-core-css' );

	wp_register_style( 'font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css');
	wp_enqueue_style( 'font-awesome' );

	wp_register_style( 'ligtbox-css', get_template_directory_uri() . '/assets/css/lightbox.min.css');
	wp_enqueue_style( 'ligtbox-css' );

	wp_register_style( 'theme-style', get_template_directory_uri() . '/assets/css/afs-themev13.css', array('woocommerce-layout', 'woocommerce-smallscreen', 'woocommerce-general'));
	wp_enqueue_style( 'theme-style' );

	wp_register_style( 'promotion-landing-pages.css', get_template_directory_uri() . '/assets/css/promotion-landing-pages.css');
	wp_enqueue_style( 'promotion-landing-pages.css' );


	//enqueue the WP copy of jQuery
	wp_enqueue_script( 'jquery' );

	wp_register_script( 'modernizer-touch', get_template_directory_uri() . '/assets/js/modernizr-touch.js');
	wp_enqueue_script( 'modernizer-touch' );

	// Forms are implemented through Form Stack; This causes conflicts with JQ mobile; Solution is to load this after FormStack scripts
	wp_register_script( 'jq-mobile', get_template_directory_uri() . '/assets/js/jquery.mobile.custom.min.js', array('jquery'), false, true);
	wp_enqueue_script( 'jq-mobile' );

	wp_register_script( 'bootstrap-core-js', get_template_directory_uri() . '/assets/js/bootstrap.js');
	wp_enqueue_script( 'bootstrap-core-js' );

	wp_register_script( 'matchheight', get_template_directory_uri() . '/assets/js/jquery.matchHeight.js', array('jquery'), false, true);
	wp_enqueue_script( 'matchheight' );

	wp_register_script( 'waypoints', get_template_directory_uri() . '/assets/js/jquery.waypoints.min.js', 'jquery');
	wp_enqueue_script( 'waypoints' );

	wp_register_script( 'jquery-jcarousel', get_template_directory_uri() . '/assets/js/jquery.jcarousel-core.js', 'jquery', false, true);
	wp_enqueue_script( 'jquery-jcarousel' );

	wp_register_script( 'lightbox-js', get_template_directory_uri() . '/assets/js/lightbox.min.js', 'jquery', false, true);
	wp_enqueue_script( 'lightbox-js' );

	wp_register_script( 'theme-scripts', get_template_directory_uri() . '/assets/js/afs-themev3.js', array('jquery', 'jquery-jcarousel'), false, true);
	wp_enqueue_script( 'theme-scripts' );

	wp_register_script( 'rlm-scripts', get_template_directory_uri() . '/assets/js/afs.campaigninfo2.min.js', array('jquery', 'js-cookie'), false, true);
	wp_enqueue_script( 'rlm-scripts' );

	if(is_shop() || is_product_category() || is_product_tag()) {

		wp_register_script( 'shop-filter', get_template_directory_uri() . '/assets/js/shopFilter.js', array(), false, true);
		wp_localize_script( 'shop-filter', 'archiveFilterMap', getFilterMapData() );
		wp_enqueue_script( 'shop-filter' );
	}

}


add_action( 'acf/input/admin_enqueue_scripts', 'my_acf_admin_enqueue_scripts' );
function my_acf_admin_enqueue_scripts() {
	// register style
    wp_register_style( 'bld-acf-styles', get_stylesheet_directory_uri() . '/assets/css/bld-acf-styles.css', false, '1.0.0' );
    wp_enqueue_style( 'bld-acf-styles' );

}

function getFilterMapData() {
	$data = get_field('product_category_filters', 'options');

	$filterMap = array_reduce($data, function($carry, $item){
		$carry[$item['product_category_slug']] = array_map(function($item) {
			return $item['value'];
		}, $item['product_category_filters']);
		return $carry;
	}, []);


	return $filterMap;
}


?>
