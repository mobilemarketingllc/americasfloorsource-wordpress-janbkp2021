<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/apple-touch-icon.png">
<link rel="icon" type="image/png" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/manifest.json">
<link rel="mask-icon" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/favicon.ico">
<meta name="msapplication-config" content="<?php bloginfo('stylesheet_directory'); ?>/assets/ico/browserconfig.xml">
<meta name="theme-color" content="#ffffff">