<?php
/*
Replace ellipses with read more link
*/
function new_excerpt_more( $more ) {
	return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . '&hellip;'.__('Read More','bldwpjs') . '</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );
