<?php
/*
Add thumbnail support and custom thumb sizes.
*/
add_theme_support( 'post-thumbnails' );

//Header Image sizes for responsiveness
add_image_size ( 'hero-full', 1920, 700, true );

add_image_size ( 'hero-lg', 1440, 700, true );
add_image_size ( 'promo-lg', 1500, 700, true );

add_image_size ( 'hero-md', 1200, 700, true );
add_image_size ( 'hero-sm', 992, 700, true );

add_image_size ( 'hero-xs', 768, 600, true );

add_image_size ( 'hero-xxs', 480, 600, true );
add_image_size ( 'promo-xxs', 480, 700, false );

//Skinny Header Sizes
add_image_size ( 'header-skinny-full', 1920, 300, true );
add_image_size ( 'header-skinny-md', 1200, 300, true );
add_image_size ( 'header-skinny-sm', 768, 200, true );




//Image sizes for Split CTA
add_image_size ( 'split-cta-lg', 960, 420, true );
add_image_size ( 'split-cta-md', 480, 420, true );


//Misc Image Sizes
add_image_size ( 'logo-sm', 250, 99999, false );


function get_acf_img($field_name, $size) {
	$image_array = get_field($field_name);
	$image_url = $image_array['url'];
	$image_title = $image_array['title'];
	$image_alt = $image_array['alt'];

	// Thumbnail dimensions
	$img_size = $size;
	$img_size_url = $image_array['sizes'][ $img_size ];
	$img_width = $image_array['sizes'][ $img_size . '-width' ];
	$img_height = $image_array['sizes'][ $img_size . '-height' ];

	return '<img src="'.$img_size_url.'" width="'.$img_width.'" height="'.$img_height.'" alt="'.$image_alt.'"	/>';

}


/********************************************

GET ACF IMAGE OBJECT ATTRIBUTES

Template usage example:

<?php
	$img = get_acf_img_attr('home_featured_image','hero');
	$img_md = get_acf_img_attr('home_featured_image','hero-md');
	$img_sm = get_acf_img_attr('home_featured_image','hero-sm');
	$img_xs = get_acf_img_attr('home_featured_image','hero-xs');
	$img_xxs = get_acf_img_attr('home_featured_image','hero-xxs');
?>

<picture>
	<source media="(min-width: 1201px)" srcset="<?php echo $img[0]; ?>">
	<source media="(min-width: 992px)" srcset="<?php echo $img_md[0]; ?>">
	<source media="(min-width: 768px)" srcset="<?php echo $img_sm[0]; ?>">
	<source media="(min-width: 481px)" srcset="<?php echo $img_xs[0]; ?>">
	<source media="(min-width: 1px)" srcset="<?php echo $img_xxs[0]; ?>">
	<img src="<?php echo $img[0]; ?>" alt="">
</picture>

********************************************/

function get_acf_img_attr($field_name, $size, $id=null) {

	if ( $id == null ) {
		$image_array = get_field($field_name);
	} else {
		$image_array = get_field($field_name,$id);
	}


	$image_url = $image_array['url'];
	$image_title = $image_array['title'];
	$image_alt = $image_array['alt'];
	$image_caption = $image_array['caption'];

	// Thumbnail dimensions
	$img_size = $size;
	$img_size_url = $image_array['sizes'][ $img_size ];
	$img_width = $image_array['sizes'][ $img_size . '-width' ];
	$img_height = $image_array['sizes'][ $img_size . '-height' ];

	return array(
		0 => $img_size_url,
		1 => $img_width,
		2 => $img_height,
		3 => $image_title,
		4 => $image_alt,
		5 => $image_caption
	);

}


//Prints responsive backgrounds Media Queries
function responsive_backgrounds_full ($backgrounds) {

	if(!count($backgrounds)) {
		return;
	}

	$CSSrules = [];

	$breakpoints = array(
		'hero-full' => '',
		'hero-lg' => '',
		'hero-md' => '',
		'hero-sm' => '',
		'hero-xs' => '',
	);

	foreach ($breakpoints as $bp => $value) {
		$temp = '';

		foreach ($backgrounds as $section => $value) {
			$temp .= '.' . $section . ' {
					background-image: url("' . $value['sizes'][$bp] . '");
				} ';
		}

		$breakpoints[$bp] = $temp;
	}


	return '
			<style>
				@media screen and (min-width: 1440px) {
					' . $breakpoints['hero-full'] . '
				}

				@media screen and (min-width: 1200px) and (max-width: 1439px) {
					' . $breakpoints['hero-lg'] . '
				}

				@media screen and (min-width: 992px) and (max-width: 1199px) {
					' . $breakpoints['hero-md'] . '
				}

				@media screen and (min-width: 768px) and (max-width: 991px) {
					' . $breakpoints['hero-sm'] . '
				}

				@media screen and (max-width: 767px) {
					' . $breakpoints['hero-xs'] . '
				}

			</style>';
}

function responsive_backgrounds_half ($backgrounds) {

	if(!count($backgrounds)) {
		return;
	}

	$CSSrules = [];

	$breakpoints = array(
		'split-cta-lg' => '',
		'split-cta-md' => ''
	);

	foreach ($breakpoints as $bp => $value) {
		$temp = '';

		foreach ($backgrounds as $section => $value) {
			$temp .= '.' . $section . ' {
					background-image: url("' . $value['sizes'][$bp] . '");
				} ';
		}

		$breakpoints[$bp] = $temp;
	}


	return '
			<style>
				@media screen and (min-width: 768px) {
					' . $breakpoints['split-cta-lg'] . '
				}

				@media screen and (max-width: 767px) {
					' . $breakpoints['split-cta-md'] . '
				}
			</style>';
}

function responsive_backgrounds_skinny ($backgrounds) {

	if(!count($backgrounds)) {
		return;
	}

	$CSSrules = [];

	$breakpoints = array(
		'header-skinny-full' => '',
		'header-skinny-md' => '',
		'header-skinny-sm' => ''
	);

	foreach ($breakpoints as $bp => $value) {
		$temp = '';

		foreach ($backgrounds as $section => $value) {
			$temp .= '.' . $section . ' {
					background-image: url("' . $value['sizes'][$bp] . '");
				} ';
		}

		$breakpoints[$bp] = $temp;
	}


	return '
			<style>
				@media screen and (min-width: 1200px)  {
					' . $breakpoints['header-skinny-full'] . '
				}

				@media screen and (min-width: 767px) and (max-width: 1199px) {
					' . $breakpoints['header-skinny-md'] . '
				}

				@media screen and (max-width: 767px) {
					' . $breakpoints['header-skinny-sm'] . '
				}

			</style>';
}