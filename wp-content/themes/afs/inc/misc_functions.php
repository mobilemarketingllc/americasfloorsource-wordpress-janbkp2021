<?php

// Enable Title Tag function
add_theme_support( "title-tag" );

/*
Remove WordPress's default padding on images with captions
*/
function remove_caption_padding( $width ) {
	return $width - 10;
}
add_filter( 'img_caption_shortcode_width', 'remove_caption_padding' );


/*
Custom featured image
*/
function custom_featured_image($size,$class) {
	$image_data = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), $size );
	$img_src = $image_data[0];
	$img_width = $image_data[1];
	$img_height = $image_data[2];
	$img_alt = get_post_meta(get_post_thumbnail_id($post->id), '_wp_attachment_image_alt', true);

	return '<img class="'.$class.'" itemprop="image" src="'.$img_src.'" alt="'.$img_alt.'" width="'.$img_width.'" height="'.$img_height.'">';
}

/*
Format Phone
*/
function format_address( $location_ID ) {
	$address = get_field("location_address", $location_ID);
	$address2 = get_field("location_address2", $location_ID);
	$city = get_field("location_city", $location_ID);
	$state = get_field("location_state", $location_ID);
	$zip = get_field("location_zip", $location_ID);

	$formatted_address = '';
	$formatted_address .= $address . '</br>';

	if($address2) {
		$formatted_address .= $address2 . '</br>';
	}

	$formatted_address .= $city . ', ' . $state['value'] . ' ' . $zip;

	return $formatted_address;
}


/*
Format Phone
*/
function format_phone( $phone_string ) {
	$phone = "(".substr($phone_string, 0, 3).") ".substr($phone_string, 3, 3)."-".substr($phone_string,6);
	return $phone;
}

/*
Replace double spaces after periods with single space (Old Folk Filter)
*/
function remove_spaces($the_content) {
    return preg_replace( '/[\p{Z}\s]{2,}/u', ' ', $the_content );
}

add_filter('the_content', 'remove_spaces');


/*
Display inline categories
*/
function display_inline_cat($cats = array(), $class = 'inline-category', $base, $query_string_var) {

	if(empty($cats)) {
		return;
	}

	$html = '<div class="' . $class . '">
				<ul class="post-categories">';

	foreach ($cats as $key => $cat) {

		$html .= '	<li>
						<a href="' . $base . '?' . $query_string_var . '=' . $cat->slug . '" rel="nofollow">' . $cat->name . '</a>
					</li>';

	}


	$html .=	'</ul>
			</div>';

	return $html;
}

/*
Display inline stars for reviews
*/
function display_stars ($num) {

	$stars = '';
	for ($i=0; $i < $num; $i++) {
		$stars .= '<i class="fa fa-star" aria-hidden="true"></i>';
	}

	return $stars;
}


// Move Yoast to bottom
function yoasttobottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


//Get All Locations
function get_all_locations() {
	$args_locations = array(
		'posts_per_page'   => -1,
		'orderby'          => 'menu_order',
		'order'            => 'ASC',
		'post_type'        => 'location',
		'post_status'      => 'publish'
	);

	return get_posts( $args_locations );

}