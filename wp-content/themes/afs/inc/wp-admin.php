<?php

// Load stylesheet for wp-admin
add_action('admin_head', 'admin_css');

function admin_css() {
	wp_register_style( 'theme-style', get_template_directory_uri() . '/assets/css/wp-admin-custom.min.css');
	wp_enqueue_style( 'theme-style' );
}

// Custom Colors in editor

function mytheme_change_tinymce_colors( $init ) {
    $default_colours = '
        "000000", "Black",
        //"993300", "Burnt orange",
        //"333300", "Dark olive",
        //"003300", "Dark green",
        //"003366", "Dark azure",
        //"000080", "Navy Blue",
        //"333399", "Indigo",
        //"333333", "Very dark gray",
        //"800000", "Maroon",
        //"FF6600", "Orange",
        //"808000", "Olive",
        //"008000", "Green",
        //"008080", "Teal",
        //"0000FF", "Blue",
        //"666699", "Grayish blue",
        //"808080", "Gray",
        //"FF0000", "Red",
        //"FF9900", "Amber",
        //"99CC00", "Yellow green",
        //"339966", "Sea green",
        //"33CCCC", "Turquoise",
        //"3366FF", "Royal blue",
        //"800080", "Purple",
        //"999999", "Medium gray",
        //"FF00FF", "Magenta",
        //"FFCC00", "Gold",
        //"FFFF00", "Yellow",
        //"00FF00", "Lime",
        //"00FFFF", "Aqua",
        //"00CCFF", "Sky blue",
        //"993366", "Brown",
        //"C0C0C0", "Silver",
        //"FF99CC", "Pink",
        //"FFCC99", "Peach",
        //"FFFF99", "Light yellow",
        //"CCFFCC", "Pale green",
        //"CCFFFF", "Pale cyan",
        //"99CCFF", "Light sky blue",
        //"CC99FF", "Plum",
        "FFFFFF", "White"
        ';
    $custom_colours = '
        "c42032", "Branded Red",
				"093c71", "Branded Blue"
        ';
    $init['textcolor_map'] = '['.$default_colours.','.$custom_colours.']';
    $init['textcolor_rows'] = 6; // expand colour grid to 6 rows
    return $init;
}
add_filter('tiny_mce_before_init', 'mytheme_change_tinymce_colors');


// Customize branding for WP login screen
function custom_login_logo() {
	echo ''
		.'<style type="text/css">'
        .'#login h1 a,'
				.'.login h1 a {'
            .'background-image: url('.get_stylesheet_directory_uri().'/assets/img/afs-logo.svg.svg);'
            .'padding-bottom: 30px;'
						.'width:320px;'
						.'height:139px;'
						.'background-size: auto;'
        .'}'
    .'</style>';
}
add_action( 'login_enqueue_scripts', 'custom_login_logo' );


//Add Custom Styles in WYSIWIG
add_filter('mce_buttons_2', 'wpb_mce_buttons_2');
function wpb_mce_buttons_2($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}


/*
* Callback function to filter the MCE settings
*/

// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );
function my_mce_before_init_insert_formats( $init_array ) {

// Define the style_formats array

    $style_formats = array(
/*
* Each array child is a format with it's own settings
* Notice that each array has title, block, classes, and wrapper arguments
* Title is the label which will be visible in Formats menu
* Block defines whether it is a span, div, selector, or inline style
* Classes allows you to define CSS classes
* Wrapper whether or not to add a new block-level element around any selected elements
*/
        array(
            'title' => 'Button - Square',
            'selector' => 'a',
            'classes' => 'btn--square',
        ),
        array(
            'title' => 'Button - Arrow (R)',
            'selector' => 'a',
            'classes' => 'btn--arrow',
        ),
        array(
            'title' => 'Button - Rounded',
            'selector' => 'a',
            'classes' => 'btn--rounded',
        ),
        array(
            'title' => 'Main Title',
            'selector' => 'h1, h2, h3, h4, h5, h6',
            'classes' => 'afs-title-lg',
        ),
        array(
            'title' => 'Title - H2 #2',
            'selector' => 'h1, h2, h3, h4, h5, h6',
            'classes' => 'scta-title',
        ),
        array(
            'title' => 'Title - H3',
            'selector' => 'h1, h2, h3, h4, h5, h6',
            'classes' => 'icta-title',
        ),
        array(
            'title' => 'Title - H3 #2',
            'selector' => 'h1, h2, h3, h4, h5, h6',
            'classes' => 'afs-title-alt',
        ),

        array(
            'title' => 'Title - H3 #3',
            'selector' => 'h1, h2, h3, h4, h5, h6',
            'classes' => 'afs-title-alt-sm',
        ),

        array(
            'title' => 'Copy Header - Light Blue',
            'selector' => 'h1, h2, h3, h4, h5, h6',
            'classes' => 'icta-subtitle',
        ),

        array(
            'title' => 'Copy Header - Dark Blue',
            'selector' => 'h1, h2, h3, h4, h5, h6',
            'classes' => 'icta-subtitle-dark',
        ),

        array(
            'title' => 'Subtitle',
            'selector' => 'h1, h2, h3, h4, h5, h6',
            'classes' => 'afs-subtitle',
        ),

        array(
            'title' => 'Lists - 2 Columns',
            'selector' => 'ul',
            'classes' => 'list-2col',
        )


    );

    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;

}

add_action( 'init', 'my_theme_add_editor_styles' );
function my_theme_add_editor_styles() {
    add_editor_style( get_template_directory_uri() . '/assets/css/custom-editor-style.min.css' );
}