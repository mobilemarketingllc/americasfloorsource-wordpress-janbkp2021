<?php
	//Page: Home
	get_header();

	$now = date("U", current_time('timestamp'));

	$promotion_start = strtotime(get_field('header_start'));
	$promotion_end = strtotime(get_field('header_end'));
	$service_title = get_field("service_title");
	$service_descriptionText  = get_field("service_description");
	$service_button_textText = get_field("service_button_text");
	$button_urlUrl = get_field("button_url");
	if($promotion_start <= $now && $now <= $promotion_end) {
		$header_bg_d = get_field("header_bg_promotion_d");
		$header_bg_promotion_d_copy = get_field("header_bg_promotion_d_copy");
		$header_bg_m = get_field("header_bg_promotion_m");
		$style = 'header-home-promotion';
		$class = get_field("header_special_class");
		
	} else {

		$header_bg_d = get_field("header_bg_promotion_d");
		$header_bg_promotion_d_copy = get_field("header_bg_promotion_m");
		$header_bg_m = get_field("header_bg_promotion_m");
		$style = 'header-home-promotion';
		$class = get_field("header_special_class");

		
	}

	$ecomEnabled = get_field('enable_ecommerce', 'options');

?>
<style>
.pro-sec-back-img-wrap{
	 background-image: url(<?php echo get_field('pro_service_background_image'); ?>);
    background-repeat: repeat-x;
	background-size: cover;
}
@media (max-width: 768px){
	.pro-sec-back-img-wrap {
		background-size: cover;
}
}
	</style>
<main id="main" role="main">
	<!-- Section: Header Home -->
	<section class="afs-section header-home <?php echo $style . ' ' . $class ?>">
		<?php
			//if ($style == 'header-home-modal') {
				include('components/element-fp-modals.php');
			//}

			//Promotion Style
			if ($style == 'header-home-promotion'):
	 ?>
		<div class="container-fluid">
			<div class="row slider-above-row">
				<div class="col-12">
				<a href="<?php echo get_field('thin_banner_url'); ?>"><?php echo get_field('thin_banner_title'); ?><i class="fa fa-chevron-right" aria-hidden="true"></i></a> 
					
				</div>
			</div>
			<?php 
			$check_slide_single =  get_field('home_page_slider_settings');
			
			$slider_code =  get_field('place_home_slider_shortcode'); 
			$header_bg_d = get_field("header_bg_promotion_d");
			$header_bg_m = get_field("header_bg_promotion_m");
		    $style = 'header-home-promotion';
		//$class = get_field("header_special_class");
			  ?>
			<div class="row">
			  <?php 
			 if ($check_slide_single == 'slide') {
			  ?>
				<?php echo do_shortcode($slider_code); } else { ?>
				<picture>
					<source media="(min-width: 768px)" srcset="<?php echo $header_bg_d['sizes']['hero-full']; ?>">
					<!-- <source media="(min-width: 768px)" srcset="<?php echo $header_bg_d['sizes']['promo-lg']; ?>"> -->
					<source media="(max-width: 767px)" srcset="<?php echo $header_bg_m['url']; ?>">
					<img src="<?php echo $header_bg_d['url']; ?>" alt="<?php echo $header_bg_d['alt']; ?>">
				</picture>
				<?php } ?>
			</div>
			
			<div class="header-content">
				<div class="header-btns">
					<button class="btn--square shop-in-store-btn" type="button" data-toggle="modal" data-target=".modal-location"><img src="https://americasfloorsource-stg.mm-dev.agency/wp-content/uploads/2020/03/Shape.png" alt="shop-in-store"height="18px" width="16px"><span>Shop in Store</span><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
					<button class="btn--square shop-at-home-btn" type="button" data-toggle="modal" data-target=".modal-appointment"><img src="https://americasfloorsource-stg.mm-dev.agency/wp-content/uploads/2020/03/mobile-van.png" alt="Shop-at-Home"><span>Shop at Home</span><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
					<?php if($ecomEnabled && $link = get_field('hero_link')): ?>
						<a class="btn--square shop-online-btn" href="<?php echo $link['url']; ?>"><img src="https://americasfloorsource-stg.mm-dev.agency/wp-content/uploads/2020/03/shopping-cart-solid.png" alt="shop-online"><?php echo $link['title']; ?><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
					<?php endif; ?>
				</div>
			</div>
		<?php

			//Normal Style
			else :
				//Print out Responsive Background CSS
				echo responsive_backgrounds_full($header_bg);
		?>

		<?php endif; ?>
		
		
		</section>
		<div class="pro-sec-back-img-wrap">
			<div class="pro-sec-main-wrap">
				<div class="container">
					<div class="pro-service-sec">
					<?php 
						  $service_tagline = get_field('service_title_tagline');
						  $service_title = get_field('service_title');
						  $service_description = get_field('service_description');
						  $service_button_text = get_field('service_button_text');
						  $button_url = get_field('button_url');
						?>
						<span><?php echo $service_tagline;?></span>
						<h2><?php echo $service_title;?> </h2>
						<p><?php echo $service_description;?></p>
						<button><a href="<?php echo $button_url;?>"><?php echo $service_button_text;?></a></button>
					</div>
					<div class="pro-service-features">
						<?php echo get_field('pro_services_description');?>
					</div>
					<div class="pro-service-img-wrap">
						<img src="<?php echo get_field('pro_service_right_side_image'); ?>" alt="pro-service-img">
					</div>
				</div>
			</div>	
		</div>
	<!-- Section: Product Bar -->
	<?php include('components/component-productbar.php'); ?>

	<!-- Section: Half Page content -->
	<?php include('components/component-hpc.php'); ?>

	<!-- Section: Split Call to Action -->
	<?php include('components/component-splitcta.php'); ?>

	<!-- Section: Testimonial -->
	<?php include('components/component-testimonial.php'); ?>

	<!-- Section: Financing Section -->
	<?php include('components/component-financingcta.php'); ?>

	<!-- Section: Inspirations Section -->
	<?php include('components/component-inspirationcta.php'); ?>

</main>

<?php

	get_footer();

?>
