<?php

	$sidebar_content = get_field("cta_sidebar", "option");

?>

	<div class="component-sidebar-about col-lg-3 col-md-3">

		<?php
			if ($sidebar_content):
				foreach ($sidebar_content as $key => $cta): ?>

					<div class="cta">
						<img src="<?php echo $cta['item_image']['url']; ?>" alt="<?php echo $cta['item_image']['alt'] ?>">
						<a class="btn--rounded" href="<?php echo $cta['item_btn_url']; ?>"><?php echo $cta['item_btn_label']; ?></a>
					</div>

		<?php
				endforeach;
			endif;
		?>

	</div>





