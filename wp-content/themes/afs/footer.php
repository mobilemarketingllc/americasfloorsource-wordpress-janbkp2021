<?php


	$bbb_images = get_field('footer_logos', 'options');
	// print_r($bbb_images);


?>

		<footer id="footer" class="afs-section afs-footer">
			<div class="container">
				<div class="footer-main row">
					<div class="main-logo col-lg-3">
						<a href="<?php bloginfo('url'); ?>">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/afs-logo.svg" alt="<?php bloginfo('name'); ?>" />
						</a>
					</div>

					<?php
						//Footer Site Map #1
						if (has_nav_menu('footer-sitemap-1')): ?>
						<div class="main-sitemap-1 col-lg-3 col-xs-6">
							<?php
								wp_nav_menu(array('theme_location' => 'footer-sitemap-1', 'walker' => new Walker_Nav_Menu));
							?>
						</div>
					<?php endif ?>

					<?php
						//Footer Site Map #2
						if (has_nav_menu('footer-sitemap-2')): ?>
						<div class="main-sitemap-2 col-lg-3 col-xs-3">
							<?php
								wp_nav_menu(array('theme_location' => 'footer-sitemap-2', 'walker' => new Walker_Nav_Menu));
							?>
						</div>
					<?php endif ?>

					<?php
						//Footer Locations
						if (has_nav_menu('footer-locations')): ?>
						<div class="main-locations col-lg-3 col-xs-3">
							<?php
								wp_nav_menu(array('theme_location' => 'footer-locations', 'walker' => new Walker_Nav_Menu));
							?>
						</div>
					<?php endif ?>

					<div class="main-display col-xl-3 col-lg-12">
						<div class="main-social">
							<h5 class="main-social-title">Stay connected with us on</h3>
							<?php social_links('list-inline'); ?>
						</div>


						<?php if (count($bbb_images)): ?>
							<div class="main-bbb">
								<?php foreach ($bbb_images as $key => $image): ?>
								<?php //print_r($image); ?>
									<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>">
								<?php endforeach ?>
							</div>
						<?php endif ?>


					</div>

				</div>

				<div class="footer-credits row">
					<div class="col-sm-12">
						<div class="copy">
							<?php echo '©' . date('Y') . ' ' . get_bloginfo('name'); ?>. All Rights Reserved.
							<ul class="list-inline">
							
								<li><a href="<?php echo get_bloginfo('url') . '/terms-of-use'; ?>">Terms of Use</a></li>
								<li><a href="<?php echo get_bloginfo('url') . '/gen-terms-cond'; ?>">General Terms and Conditions of Sale</a></li>
								<li><a href="<?php echo get_bloginfo('url') . '/privacy-policy'; ?>">Privacy Policy</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>
		
       

<script>
jQuery(document).ready(function($) {
if (jQuery('ul li').hasClass('Manufacturer_Warranty'))
{
	jQuery(".installation_li").insertAfter(".Manufacturer_Warranty");
   // jQuery(".warranty_link").insertAfter(".installation_li");
	jQuery(".install_link").insertAfter(".Manufacturer_Warranty .value");
}
else
{
	jQuery(".installation_li").insertAfter(".woocommerce div.product .product_attributesList li:last-child");
	//jQuery(".install_link").insertAfter(".woocommerce div.product .product_attributesList li:last-child");
  //  jQuery(".warranty_link").insertAfter(".woocommerce div.product .product_attributesList li:last-child .value");
}

});
</script>
     <?php if ($post->ID =='7051') { ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	
		<script src="https://calculator.measuresquare.com/scripts/jquery-m2FlooringCalculator.js"></script>

        <script>
            $(function () {				
                $('#calculateBtn').m2Calculator({
                    measureSystem: "Imperial",            
                    thirdPartyName: "America's Floor Source", 
                    thirdPartyEmail: "marketing@americasfloorsource.com",  // if showDiagram = false, will send estimate data to this email when user click Email Estimate button
                    showCutSheet: false, // if false, will not include cutsheet section in return image
                    showDiagram: true,  // if false, will close the popup directly
                  /*  product: {
                        type: "Carpet",
                        name: "Carpet 1",
                        width: "6'0\"",
                        length: "150'0\"",
                        horiRepeat: "3'0\"",
                        vertRepeat: "3'0\"",
                        horiDrop: "",
                        vertDrop: ""
                    },
					*/
                    cancel: function () {
                        //when user closes the popup without calculation.
                    },
                    callback: function (data) {
                        //json format, include user input, usage and base64image
                        $("#callback").html(JSON.stringify(data));   
                        console.log(data.input)
                        $("#usageText").val(data.usage);    
                        $("#image").attr("src", data.img);  //base64Image 
						//window.location.href = "https://www.americasfloorsource.com/thank-you/";
                    }
                });	

				
            });
		 </script>
		 <?php } ?>	
		<?php wp_footer(); ?>

		

	</body>
</html>
