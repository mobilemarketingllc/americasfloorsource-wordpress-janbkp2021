<?php

	get_header();

	$product_title = get_field('inspiration_product') ?: get_the_title();
	$product_image = get_field('inspiration_image');
	$product_specs = get_field('inspiration_spec');

	//Get Categories
	$inspiration_categories = wp_get_post_terms(
		get_the_ID(),
		array('room_cat','product_type'),
		array(
			'orderby' => 'taxonomy',
			'order' => 'ASC'
		)
	);
	// print_r($inspiration_categories);

	$product_type = array_filter($inspiration_categories, function($v){

		if($v->taxonomy == 'product_type') {
			return true;
		}

		return false;

	}, ARRAY_FILTER_USE_BOTH);

	//Tabbed Content
	$inspiration_reviews =  get_field('inspiration_tabs_reviews');
	$inspiration_qa =  get_field('inspiration_tabs_qa');
	$inspiration_install =  get_field('inspiration_tabs_install');

	if(count($product_type)) {
		//Promotions
		$now = date("Y-m-d H:i:s", current_time('timestamp'));

		//Filter for Active Promotions
		$args = array(
			'post_type' => 'promotion',
			'order' => 'DESC',
			'orderby' => 'date',
			'tax_query' => array(
				array(
					'taxonomy' => 'product_type',
					'field' => 'slug',
					'terms' => $product_type[0]->slug
				)
			),
			'meta_query' => array(
				'relation'	=> 'AND',
		        array(
		            'key' => 'promo_start',
		            'value' => $now,
		            'compare' => '<=',
		            'type' => 'DATE'
		        ),
		        array(
		            'key' => 'promo_end',
		            'value' => $now,
		            'compare' => '>=',
		            'type' => 'DATE'
		        )
		    )
		);

		$inspiration_promo = new WP_Query($args);
	}

	$post_header = get_field("header_bg");

?>

<main id="main" role="main">

	<!-- Header: Large -->
	<section class="afs-post-header">
		<picture>
			<source media="(min-width: 1440px)" srcset="<?php echo $post_header['sizes']['hero-full']; ?>">
			<source media="(min-width: 1200px)" srcset="<?php echo $post_header['sizes']['hero-lg']; ?>">
			<source media="(min-width: 992px)" srcset="<?php echo $post_header['sizes']['hero-md']; ?>">
			<source media="(min-width: 768px)" srcset="<?php echo $post_header['sizes']['hero-sm']; ?>">
			<source media="(min-width: 1px)" srcset="<?php echo $post_header['sizes']['hero-xs']; ?>">
			<img src="<?php echo $post_header['url']; ?>" alt="<?php echo $post_header['alt']; ?>">
		</picture>
	</section>


	<!-- Section: Content + Accordian -->
	<section class="afs-section afs-inspiration">
		<div class="container">
			<div class="row">
				<div class="inspiration-social col-lg-6 col-md-6 col-sm-12 col-xs-12">

					<?php
						//Social settings
						$social_title = get_field("social_title") ?: $product_title;
						$social_description = get_field("social_description") ?: '';
						$social_url = get_field("social_url") ?: get_the_permalink();
						$social_media = get_field("social_photo")['sizes']['hero-xs'] ?: $post_header['sizes']['hero-sm'];

						$social_categories = get_field("social_categories");

					?>

					<ul class="social-actions">
						<li>Like this look?</li>
						<li class="social-pinterest">
							<a 	data-pin-do="buttonBookmark"
								data-pin-custom="true"
								data-pin-media="<?php echo $post_header['sizes']['hero-xs'] ?>"
								href="https://www.pinterest.com/pin/create/button/">
								<i class="fa fa-pinterest" aria-hidden="true"></i>
							</a>
							<script async defer src="//assets.pinterest.com/js/pinit.js"></script>
						</li>
						<li class="social-houzz">
							<a class="houzz-share-button"
								data-url="<?php echo $social_url ?>"
								data-hzid="40238"
								data-title="<?php echo $social_title ?>"
								data-img="<?php echo $social_media ?>"
								data-desc="<?php echo $social_description ?>"
								data-showcount="0"
								href="https://www.houzz.com">
								<i class="fa fa-houzz" aria-hidden="true"></i>
							</a>
							<script>(function(d,s,id){if(!d.getElementById(id)){var js=d.createElement(s);js.id=id;js.async=true;js.src="//platform.houzz.com/js/widgets.js?"+(new Date().getTime());var ss=d.getElementsByTagName(s)[0];ss.parentNode.insertBefore(js,ss);}})(document,"script","houzzwidget-js");</script>
						</li>
						<li class="social-fb">
							<div class="fb-share-button" data-href="<?php echo $social_url ?>" data-layout="button" data-size="large" data-mobile-iframe="false"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"><i class="fa fa-facebook" aria-hidden="true"></i></a></div>
							<div id="fb-root"></div>
							<script>(function(d, s, id) {
							  var js, fjs = d.getElementsByTagName(s)[0];
							  if (d.getElementById(id)) return;
							  js = d.createElement(s); js.id = id;
							  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9";
							  fjs.parentNode.insertBefore(js, fjs);
							}(document, 'script', 'facebook-jssdk'));</script>
						</li>
					</ul>
				</div>

				<div class="inspiration-title col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<h2 class="afs-title-lg"><?php echo $product_title; ?></h2>
					<?php echo display_inline_cat($inspiration_categories, 'inline-category', get_bloginfo('url') . '/inspiration-gallery/', 'type'); ?>
				</div>
			</div>

			<div class="row">
				<div class="inspiration-image col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<img src="<?php echo $product_image['sizes']['split-cta-md'] ?>" alt="<?php echo $product_image['alt']; ?>">
				</div>

				<?php if ($product_specs): ?>
					<div class="inspiration-specs col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="table-container">
							<table>
								<?php foreach ($product_specs as $key => $spec): ?>
									<tr>
										<td class="spec-label afs-title-sm"><?php echo $spec['spec_label']; ?></td>
										<td class="spec-value"><?php echo $spec['spec_value']; ?></td>
									</tr>
								<?php endforeach ?>

							</table>
						</div>
					</div>
				<?php endif ?>
			</div>

			<div class="row">
				<?php if (get_field('inspiration_content_1')): ?>
					<div class="inspiration-content-1 col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<?php the_field('inspiration_content_1'); ?>
					</div>
				<?php endif ?>

				<?php if (get_field('inspiration_content_2')): ?>
					<div class="inspiration-content-2 col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<?php the_field('inspiration_content_2'); ?>
					</div>
				<?php endif ?>
			</div>

			<?php if ($inspiration_reviews || $inspiration_qa || $inspiration_install || $inspiration_promo->have_posts()): ?>
				<div class="row">

					<div class="inspiration-tabs afs-tabs col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<!-- Nav tabs -->
						<ul class="tabs">
							<?php
								$i = 0;

								if ($inspiration_reviews):

							?>
									<li <?php echo ($i == 0) ? ' class="active"' : ''; ?> rel="<?php echo 'tab' . $i ?>">Customer Reviews</li>
							<?php
									$i++;
								endif;
							?>

							<?php if ($inspiration_qa): ?>
									<li <?php echo ($i == 0) ? ' class="active"' : ''; ?> rel="<?php echo 'tab' . $i ?>">Questions & Answers</li>
							<?php
									$i++;
								endif;
							?>

							<?php if ($inspiration_install): ?>
									<li <?php echo ($i == 0) ? ' class="active"' : ''; ?> rel="<?php echo 'tab' . $i ?>">Installation</li>
							<?php
									$i++;
								endif;
							?>

							<?php if (isset($inspiration_promo) && $inspiration_promo->have_posts()): ?>
									<li <?php echo ($i == 0) ? ' class="active"' : ''; ?> rel="<?php echo 'tab' . $i ?>">Promotions</li>
							<?php
									$i++;
								endif;
							?>

						</ul>

						<!-- Tab panes -->
						<div class="tab-container">
							<?php
								$i = 0;

								if ($inspiration_reviews):
							?>
								<!-- Reviews -->
								<div class="tab-drawer-heading<?php echo ($i == 0) ? ' d-active' : ''; ?>" rel="<?php echo 'tab' . $i ?>"><h5>Customer Reviews</h5></div>
							    <div id="<?php echo 'tab' . $i ?>" class="tab-content">
									<?php foreach ($inspiration_reviews as $key => $review): ?>
										<div class="afs-review">
											<div class="review-header">
												<h3 class="review-name"><?php echo $review['review_name'] ?></h3>
												<span class="review-stars"><?php echo display_stars($review['review_stars']); ?></span>
											</div>
											<div class="review-body"><?php echo $review['review_body'] ?></div>
										</div>
									<?php endforeach ?>
								</div>
							<?php
									$i++;
								endif;
							?>


							<?php if ($inspiration_qa): ?>
								<!-- Questions and Answers -->
								<div class="tab-drawer-heading<?php echo ($i == 0) ? ' d-active' : ''; ?>" rel="<?php echo 'tab' . $i ?>"><h5>Questions & Answers</h5></div>
							    <div id="<?php echo 'tab' . $i ?>" class="tab-content">
									<?php foreach ($inspiration_qa as $key => $qa): ?>
										<div class="afs-qa">
											<h4 class="qa-q"><?php echo $qa['qa_q']; ?></h4>
											<div class="qa-a"><?php echo $qa['qa_a']; ?></div>
										</div>
									<?php endforeach ?>
								</div>
							<?php
									$i++;
								endif;
							?>



							<?php if ($inspiration_install): ?>
								<!-- Installation -->
								<div class="tab-drawer-heading<?php echo ($i == 0) ? ' d-active' : ''; ?>" rel="<?php echo 'tab' . $i ?>"><h5>Installation</h5></div>
							    <div id="<?php echo 'tab' . $i ?>" class="tab-content">
									<div class="afs-CAS-1">
										<?php echo $inspiration_install; ?>
									</div>
								</div>
							<?php
									$i++;
								endif;
							?>




							<?php if ($inspiration_promo->have_posts()): ?>
								<!-- Promotions -->
								<div class="tab-drawer-heading<?php echo ($i == 0) ? ' d-active' : ''; ?>" rel="<?php echo 'tab' . $i ?>"><h5>Promotions</h5></div>
							    <div id="<?php echo 'tab' . $i ?>" class="tab-content">
									<?php

										while($inspiration_promo->have_posts()):

											$inspiration_promo->the_post();
											$promo_image = get_field('promo_image');
											$promo_links = get_field('promo_links');
										?>

											<div class="promotion-card promotion-card-horizontal">
												<div class="promotion-card-image">
													<img src="<?php echo $promo_image['sizes']['split-cta-md']; ?>" alt="<?php echo $promo_image['alt']; ?>">
												</div>
												<div class="promotion-card-info">
													<div class="promotion-card-info-inner">
														<h4 class="afs-title-md"><?php the_title(); ?></h4>
														<?php echo get_field("promo_content"); ?>
													</div>
												</div>
											</div>

										<?php endwhile; ?>
								</div>
							<?php
									$i++;
								endif;
							?>

					    </div>
					</div>

				</div>
			<?php endif ?>
		</div>
	</section>

	<!-- Section: Split Call to Action -->
	<?php include('components/component-splitcta.php'); ?>

	<!-- Section: Financing Section -->
	<?php include('components/component-financingcta.php'); ?>

</main>

<?php

	get_footer();

?>
