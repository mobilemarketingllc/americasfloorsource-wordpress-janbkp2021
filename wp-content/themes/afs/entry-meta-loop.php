<p class="entry-meta">

<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a>

 |
<?php
	echo '<time class="published" datetime="'.get_the_time('c').'">'
		.get_the_time("D. M d, Y", $post->ID)
		.'</time>';

	if (get_the_category_list() != '' || get_the_tag_list() != '') {
		echo ' | ';
	}

	if (get_the_category_list() != '') {
		echo 'Category: '.get_the_category_list( ', ' );
	}

	if (get_the_category_list() != '' && get_the_tag_list() != '') {
		echo ' | ';
	}

	if (get_the_tag_list() != '') {
		echo 'Tag: '.get_the_tag_list( '', ', ' );
	}
?>
</p>
