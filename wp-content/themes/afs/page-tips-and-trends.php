<?php
	get_header();

	$filter_bg['component-filter'] = get_field('filter_bar_bg', 'option');

	//Get the page URL so we can correctly link/filter categories
	$base_url = get_the_permalink();



	//Get Tips and Trends
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$args = array(
		'post_type' => 'tips_and_trends',
		'posts_per_page' => 8,
		'paged' => $paged,
		'order' => 'DESC',
		'orderby' => 'date'
	);

	//If 'type' is present then we filter
	if(!empty($_GET['type'])) {
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'tt_cat',
				'field' => 'slug',
				'terms' => sanitize_text_field( $_GET['type'] )
			)
		);
	}

	$tat = new WP_Query($args);

?>

<main id="main" role="main">

	<!-- Section: Header Large -->
	<?php include('components/header-skinny.php'); ?>

	<!-- Section: Filter -->
	<section class="afs-section component-filter">
		<?php echo responsive_backgrounds_skinny($filter_bg); ?>

		<div class="container">
			<div class="row">
				<div class="filter-container col-xs-12">
					<a class="btn-viewall pull-left" href="<?php echo $base_url; ?>?type=tips" rel="nofollow">View Tips</a>
					<a class="btn-viewall pull-left" href="<?php echo $base_url; ?>?type=trends" rel="nofollow">View Trends</a>
					<a class="btn-viewall pull-right" href="<?php echo $base_url; ?>">View All</a>
				</div>
			</div>
		</div>
	</section>

	<?php if ($tat->have_posts()): ?>
		<!-- Section: Tips & Trends Grid -->
		<section class="afs-section afs-post-grid afs-tt-grid">

			<div class="container inspirations-container">
				<div class="row">

					<div class="afs-post-grid-header afs-CAS-1 col-sm-12">
						<?php the_field("post_header"); ?>
					</div>


					<?php
						while($tat->have_posts()) :

							$tat->the_post();

							include('components/element-tt-card.php');

						endwhile;
					?>
				</div>

				<?php

					$pagination = paginate_links( array(
						'base' => str_replace( 9999, '%#%', esc_url( get_pagenum_link( 9999 ) ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => $tat->max_num_pages
					));

					if ($pagination): ?>
					<div class="row">
						<div class="component-pagination col-lg-12">
							<?php echo $pagination ?>
						</div>
					</div>

				<?php
					endif;
				?>
			</div>
		</section>

	<?php
		else :
			//No posts returned
			include('components/component-no-grid.php');

		endif;

		wp_reset_postdata()
	?>


	<!-- Section: Split Call to Action -->
	<?php include('components/component-splitcta.php'); ?>

	<!-- Section: Half Page content -->
	<?php include('components/component-inspirationcta.php'); ?>

</main>

<?php

	get_footer();

?>
