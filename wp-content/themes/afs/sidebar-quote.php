<?php if (get_field("sidebar-form", "option")): ?>

	<aside id="sidebar-quote" class="sb-quote">
		<span class="sb-quote-close fa-stack fa-lg">
			<i class="fa fa-circle fa-stack-2x"></i>
			<i class="fa fa-times fa-stack-1x"></i>
		</span>

		<div class="sb-quote-tag">
			Request a Quote
		</div>
		<div class="sb-quote-content">
			<?php the_field("sidebar-form", "option"); ?>
		</div>

	</aside>

<?php endif ?>