<?php
	get_header();

	$formID = get_field("mfs_form_id");

	$mfs_items = get_field("mfs_content");
?>

<main id="main" role="main">

	<!-- Section: Header Large -->
	<?php include('components/header-skinny.php'); ?>

	<!-- Section: Schedule Form -->
	<section id="afs-mfs-form" class="afs-section afs-mfs-form">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<?php the_field("mfs_form_id"); ?>
				</div>
			</div>
		</div>
	</section>

	<section class="afs-section afs-mfs">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="afs-title-lg"><?php the_field("mfs_title"); ?></h2>
				</div>

				<?php if ($mfs_items): ?>
					<?php foreach ($mfs_items as $key => $item): ?>
						<div class="mfs-item col-lg-4 col-md-4">
							<img src="<?php echo $item['item_icon']['url'] ?>" alt="<?php echo $item['item_icon']['alt'] ?>">
							<?php echo $item['item_content']; ?>
						</div>
					<?php endforeach ?>
				<?php endif ?>

				<div class="mfs-cta col-lg-12">
					<a class="btn--square scroll-to" href="#afs-mfs-form">Schedule An Appointment</a>
				</div>

			</div>
		</div>
	</section>


	<!-- Section: Half Page content -->
	<?php include('components/component-hpc.php'); ?>

</main>

<?php

	get_footer();

?>
