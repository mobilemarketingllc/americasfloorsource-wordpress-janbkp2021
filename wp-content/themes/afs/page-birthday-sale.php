<?php
	get_header();

	$header_bg_d = get_field("header_bg_promotion_d");
	$header_bg_m = get_field("header_bg_promotion_m");

	$backgrounds['afs-birthday-form'] = get_field('birthday_bg');
	$backgrounds['afs-birthday-fin'] = get_field('financing_bg_image', 'option');

	$radios = get_field('birthday_radio');

	$now = date("Y-m-d H:i:s", current_time('timestamp'));

	//Filter for Active Promotions
	$args = array(
		'post_type' => 'promotion',
		'posts_per_page' => -1,
		'order' => 'DESC',
		'orderby' => 'date',
		'tax_query' => array(
			array(
				'taxonomy' => 'category',
				'field'    => 'slug',
				'terms'    => 'birthday-sale',
			),
		)
	);

	$promotions = new WP_Query($args);

?>

<main id="main" role="main">

	<!-- Section: Header Home -->
	<section class="afs-section header-home header-home-promotion">
		<picture>
			<source media="(min-width: 1200px)" srcset="<?php echo $header_bg_d['sizes']['hero-full']; ?>">
			<source media="(min-width: 768px)" srcset="<?php echo $header_bg_d['sizes']['promo-lg']; ?>">
			<source media="(min-width: 480px)" srcset="<?php echo $header_bg_m['sizes']['hero-xs']; ?>">
			<source media="(min-width: 1px)" srcset="<?php echo $header_bg_m['sizes']['promo-xxs']; ?>">
			<img src="<?php echo $header_bg_d['url']; ?>" alt="<?php echo $header_bg_d['alt']; ?>">
		</picture>
	</section>

	<?php if ($promotions->have_posts()): ?>

		<!-- Section: Inspiration Grid -->
		<section class="afs-section afs-post-grid afs-promotions-grid afs-birthday-promotions">

			<div class="container">
				<div class="row">

					<div class="afs-post-grid-header afs-CAS-1 col-sm-12">
						<?php the_field("post_header"); ?>
					</div>

				</div>
				<div class="row afs-birthday-promotions-container">
					<?php
						while($promotions->have_posts()) :
							$promotions->the_post();

							$promo_image = get_field('promo_image');

					?>
						<div class="promotion-card col-lg-3 col-md-3 col-sm-4 col-xs-6">
 							<div class="promotion-card-image">
								<img src="<?php echo $promo_image['sizes']['split-cta-md']; ?>" alt="<?php echo $promo_image['alt']; ?>">
							</div>
							<div class="promotion-card-info">
								<div class="promotion-card-info-inner">
									<h4 class="afs-title-md"><?php the_title(); ?></h4>
									<?php echo get_field("promo_content"); ?>
								</div>
							</div>
						</div>

					<?php
						endwhile;
					?>
				</div>
			</div>
		</section>

	<?php
		else :
			//No posts returned
			include('components/component-no-grid.php');

		endif;

		wp_reset_postdata()
	?>

	<!-- Section: Enter to Win -->
	<section class="afs-section afs-birthday-form">
		<div class="container">
			<div class="row">
				<div class="form-content
							col-lg-6 col-lg-offset-6
							col-md-8 col-md-offset-4
							col-sm-10 col-sm-offset-2 col-xs-12">
					<div class="form-container">
						<div class="form-header">
							<div class="form-header-l1">
								<p class="form-header-l1-main"><?php the_field('birthday_form_header_1'); ?></p>
							</div>
							<div class="form-header-l2"><?php the_field('birthday_form_header_2'); ?></div>
						</div>
						<div class="form-form">
							<?php the_field('birthday_form'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Section: Enter to Win -->
	<section class="afs-section afs-birthday-radio">
		<div class="container">
			<div class="row">
				<div class="col-lg-12"><h3 class="afs-title-lg">Radio Station Events</h3></div>
				<?php foreach ($radios as $key => $radio): ?>

					<div class="station col-sm-4">
						<img class="station-icon" src="<?php echo $radio['radio_icon']['url']; ?>" alt="<?php echo $radio['radio_icon']['alt']; ?>">
						<div class="station-content">
							<?php echo $radio['radio_content'] ?>
						</div>
					</div>

				<?php endforeach ?>
			</div>
		</div>
	</section>


	<!-- Section: Financing Section -->
	<?php include('components/component-financingcta.php'); ?>


	<?php echo responsive_backgrounds_full ($backgrounds) ?>

</main>

<?php

	get_footer();

?>
