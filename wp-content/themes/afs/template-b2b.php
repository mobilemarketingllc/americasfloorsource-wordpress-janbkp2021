<?php
	// Template Name: B2B

	get_header();

	$testimonial_cat = 'b2b';
?>

<main id="main" role="main">

	<!-- Section: Header Large -->
	<?php include('components/header-skinny.php'); ?>

	<!-- Section: Content with Sidebar -->
	<section class="afs-section afs-b2b">
		<div class="container">
			<div class="row">
				<?php
					if (has_nav_menu('b2b-template') ) : ?>
						<div class="component-sidebar-menu col-lg-3 col-md-3">
							<div class="dropdown">
								<button class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									Navigation
								</button>
								<?php
									wp_nav_menu(array('theme_location' => 'b2b-template', 'menu_class' => 'dropdown-menu'));
								?>
							</div>
						</div>
				<?php
					endif;
				?>

				<div class="afs-b2b-main afs-CAS-1 col-lg-9 col-md-9">
					<?php the_field("b2b_content"); ?>
				</div>

			</div>
		</div>
	</section>

	<!-- Section: Testimonial -->
	<?php include('components/component-testimonial.php'); ?>

	<!-- Section: B2B Form -->
	<section id="afs-b2b-form" class="afs-section afs-b2b-form">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<?php the_field("b2b_form", "option"); ?>
				</div>
			</div>
		</div>
	</section>


</main>

<?php

	get_footer();

?>
