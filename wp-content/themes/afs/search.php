<?php get_header(); ?>

<main id="main" role="main">
	<article id="page-<?php the_id(); ?>" class="page">
		<header class="page-header">
			<div class="container">
				<h1 class="heading">Search For "<?php echo get_search_query(); ?>"</h1>
			</div>
		</header>
		<section class="page-content">
			<div class="container">

				<div class="row">

					<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
						<?php echo get_search_form(); ?>
					</div>

				</div>

				<div class="row">

					<div id="search-results" class="col-xs-12">

						<?php
							if ( have_posts() ) {
								while ( have_posts() ) {
									the_post();
						?>

						<section id="article-<?php the_id(); ?>" class="article search-result clearfix">

							<p class="search-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
							<p class="search-link"><a href="<?php the_permalink(); ?>"><?php the_permalink(); ?></a></p>
							<p class="search-excerpt"><?php the_excerpt(); ?></p>

						</section>

						<?php
								} //endwhile;
							} else {
							?>
								<section id="article-<?php the_id(); ?>" class="article search-result-none clearfix">
									<h2 class="text-center">Nothing Found</h2>
									<p class="search-title text-center">
										No match was found for the search term entered. You may want to try broadening your search or searching with an alternate phrase.
									</p>
								</section>
							<?php
							}
						?>

					</div>

				</div>

				<div class="row">

					<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
						<?php echo get_search_form(); ?>
					</div>

				</div>

			</div>
		</section>
	</article>
</main>

<?php get_footer(); ?>
