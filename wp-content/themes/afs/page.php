<?php get_header(); ?>

<?php
	if ( have_posts() ) :
		while ( have_posts() ) : the_post();
?>

<main id="main" role="main">
	<section class="afs-page">
		<div class="container">
			<?php the_content(); ?>
		</div>
	</section>
</main>

<?php
		endwhile;
	endif;
?>

<?php get_footer(); ?>
