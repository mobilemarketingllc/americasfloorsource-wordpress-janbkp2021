<form class="searchform" action="<?php bloginfo('url'); ?>" method="get">
	<div class="input-group">
		<input type="text" name="s" id="search" class="form-control" placeholder="Search for..." value="<?php the_search_query(); ?>">
		<span class="input-group-btn">
			<input type="submit" class="btn btn-success" value="Search">
		</span>
	</div>
</form>
