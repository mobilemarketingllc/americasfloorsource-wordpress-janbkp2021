<?php

	get_header();
	$content = get_field("about_content");

?>

<main id="main" role="main">

	<!-- Section: Header Large -->
	<?php include('components/header-large.php'); ?>

	<!-- Section: Content with Sidebar -->
	<?php if ($content): ?>
		<section class="afs-section afs-about">
			<div class="container">
				<div class="row">
					<div class="col-lg-9">
						<?php
							foreach ($content as $key => $section):

								switch ($section['acf_fc_layout']) {
									case 'layout_simple':
										include('components/component-layout-simple.php');

										break;

									case 'layout_3col':
										include('components/component-layout-3col.php');

										break;

									case 'layout_locations':
										include('components/component-layout-locations.php');

										break;


								}

							endforeach;
						?>
					</div>
				</div>
			</div>
		</section>
	<?php endif ?>

	<!-- Section: Content with Sidebar -->
	<section class="afs-section afs-lar">
		<div class="container">
			<div class="row">
				<div class="col-lg-9">
					<h2 class="afs-title-lg">Leave a Review</h2>

					<?php
						$locations_array = get_all_locations();

						$social_icons = array(
							'angie' => '<img src="' . get_bloginfo('stylesheet_directory') . '/assets/img/review-angie.jpg" alt="Angie\'s List">',
							'bbb' => '<img src="' . get_bloginfo('stylesheet_directory') . '/assets/img/review-bbb.png" alt="Better Business Bureau">',
							'google' => '<img src="' . get_bloginfo('stylesheet_directory') . '/assets/img/review-google.svg" alt="Google Plus">',
							'yelp' => '<img src="' . get_bloginfo('stylesheet_directory') . '/assets/img/review-yelp.png" alt="Yelp">',
							'yahoo' => '<img src="' . get_bloginfo('stylesheet_directory') . '/assets/img/review-yahoo.png" alt="Yahoo">',
							'citysearch' => '<img src="' . get_bloginfo('stylesheet_directory') . '/assets/img/review-cs.png" alt="CitySearch">',
							'fb' => '<img src="' . get_bloginfo('stylesheet_directory') . '/assets/img/review-facebook.jpg" alt="Facebok">',
							'houzz' => '<img src="' . get_bloginfo('stylesheet_directory') . '/assets/img/review-houzz.png" alt="Houzz">'

						);

						foreach ($locations_array as $key => $location):
							$review_urls = get_field('location_review_urls', $location->ID);

							if ($review_urls): ?>
								<div class="location location-reviews col-lg-4 col-md-6 col-sm-6 col-xs-6">
									<h3 class="afs-title-md"><?php the_field("location_shortname", $location->ID) ?></h3>
									<ul class="location-reviews-links">
										<?php foreach ($review_urls as $key => $network): ?>
											<li><a href="<?php echo $network['item_url'] ?>" target="_blank"><?php echo $social_icons[$network['item_network']['value']] ?></a></li>
										<?php endforeach ?>
									</ul>
								</div>
							<?php endif ?>



					<?php
						endforeach;
					?>
				</div>
			</div>
		</div>
	</section>


	<!-- Section: Testimonial -->
	<?php include('components/component-testimonial.php'); ?>

</main>

<?php

	get_footer();

?>
