<?php
	get_header();

	$filter_bg['component-filter'] = get_field('filter_bar_bg', 'option');

	//Get the page URL so we can correctly link/filter categories
	$base_url = get_the_permalink();

	//Get Inspirations
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$args = array(
		'post_type' => 'inspirations',
		'posts_per_page' => 8,
		'paged' => $paged,
		'order' => 'DESC',
		'orderby' => 'date'
	);

	$test1 = '';
	$test2 = '';

	if(!empty($_GET['room']) || !empty($_GET['ptype']) || !empty($_GET['type'])) {

		$args['tax_query'] = array(
			'relation' => 'AND'
		);

		if(!empty($_GET['ptype'])) {
			$test1 = $_GET['ptype'];

			$args['tax_query'][] = array(
				'taxonomy' => 'product_type',
				'field' => 'slug',
				'terms' => sanitize_text_field( $_GET['ptype'] )
			);

		}

		if(!empty($_GET['room'])) {
			$test2 = $_GET['room'];

			$args['tax_query'][] = array(
				'taxonomy' => 'room_cat',
				'field' => 'slug',
				'terms' => sanitize_text_field( $_GET['room'] )
			);

		}

		if(!empty($_GET['type'])) {
			$test1 = $_GET['type'];
			$test2 = $_GET['type'];

			$args['tax_query'] = array(
				'relation' => 'OR'
			);

			$args['tax_query'][] = array(
				'taxonomy' => 'room_cat',
				'field' => 'slug',
				'terms' => sanitize_text_field( $_GET['type'] )
			);

			$args['tax_query'][] = array(
				'taxonomy' => 'product_type',
				'field' => 'slug',
				'terms' => sanitize_text_field( $_GET['type'] )
			);

		}
	}
	// print_r($args);

	$inspirations = new WP_Query($args);


	// Filtering Mechanism
	// Get all terms
	$terms = get_terms(array(
		'taxonomy' => array('room_cat', 'product_type'),
		'orderby' => 'order_menu',
		'order' => 'ASC',
		'hide_empty' => 0
	));
	// print_r($terms);

	$terms_filtered = array();
	foreach ($terms as $key => $t) {
		$terms_filtered[$t->taxonomy][] = $t;
	}
	// print_r($terms_filtered);

?>

<main id="main" role="main">

	<!-- Section: Header Large -->
	<?php include('components/header-large.php'); ?>

	<!-- Section: Filter -->
	<section class="afs-section component-filter">
		<?php echo responsive_backgrounds_skinny($filter_bg); ?>

		<div class="container">
			<div class="row">
				<div class="filter-container col-xs-12">
					<form id="inspiration-gallery-filter" class="form-inline double-filter" action="<?php the_permalink(); ?>" method="GET">
						<div class="form-container">
							<div class="dropdown">
								<button class="dropdown-placeholder" type="button" data-toggle="dropdown" data-placeholder="Material Type" aria-haspopup="true" aria-expanded="false">Material Type</button>
								<ul class="dropdown-menu">
									<?php foreach ($terms_filtered['product_type'] as $key => $value): ?>
										<li <?php echo ($value->slug == $test1) ? 'class="selected"' : '' ; ?> data-value="<?php echo $value->slug; ?>"><?php echo $value->name; ?></li>
									<?php endforeach ?>
								</ul>
								<input type="text" name="ptype" value="<?php echo (isset($_GET['ptype'])) ? $_GET['ptype'] : '' ; ?>">
							</div>

							<div class="dropdown">
								<button class="dropdown-placeholder" type="button" data-toggle="dropdown" data-placeholder="Room Type" aria-haspopup="true" aria-expanded="false">Room Type</button>
								<ul class="dropdown-menu">
									<?php foreach ($terms_filtered['room_cat'] as $key => $value): ?>
										<li <?php echo ($value->slug == $test2) ? 'class="selected"' : '' ; ?> data-value="<?php echo $value->slug; ?>"><?php echo $value->name; ?></li>
									<?php endforeach ?>
								</ul>
								<input type="text" name="room" value="<?php echo (isset($_GET['room'])) ? $_GET['room'] : '' ; ?>">
							</div>
						</div>
						<div class="form-action">
							<button type="submit" class="btn--rounded btn--nocaret">Filter</button>
							<span class="dropdown-clear" title="Clear Filters"><i class="fa fa-times" aria-hidden="true"></i>Clear</span>
						</div>
						<a class="btn-viewall" href="<?php the_permalink(); ?>">View All</a>
					</form>
				</div>
			</div>
		</div>
	</section>

	<?php if ($inspirations->have_posts()): ?>

		<!-- Section: Inspiration Grid -->
		<section class="afs-section afs-post-grid afs-inspiration-grid">

			<div class="container inspirations-container">
				<div class="row">

					<div class="afs-post-grid-header afs-CAS-1 col-sm-12">
						<?php the_field("post_header"); ?>
					</div>

					<?php
						while($inspirations->have_posts()) :

							$inspirations->the_post();

							include('components/element-ig-card.php');

						endwhile;
					?>

				</div>

				<?php

					$pagination = paginate_links( array(
						'base' => str_replace( 9999, '%#%', esc_url( get_pagenum_link( 9999 ) ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => $inspirations->max_num_pages
					));

					if ($pagination): ?>
					<div class="row">
						<div class="component-pagination col-lg-12">
							<?php echo $pagination ?>
						</div>
					</div>

				<?php
					endif;
				?>
			</div>
		</section>

	<?php else :
			//No posts returned
			include('components/component-no-grid.php');

		endif;

		wp_reset_postdata()
	?>


	<!-- Section: Split Call to Action -->
	<?php include('components/component-splitcta.php'); ?>

	<!-- Section: Half Page content -->
	<?php include('components/component-hpc.php'); ?>

</main>

<?php

	get_footer();

?>
