<?php get_header(); ?>

<?php
	if ( have_posts() ) :
		while ( have_posts() ) : the_post();
?>

<main id="main" role="main">
	<article id="page-<?php the_id(); ?>" class="page" itemscope itemtype="http://schema.org/Article">
		<header class="page-header">
			<div class="container">
				<h1 class="heading" itemprop="name"><?php the_title(); ?></h1>
			</div>
		</header>
		<?php
		if ( has_post_thumbnail() ) {
		?>
		<div class="feature-image">
			<?php echo custom_featured_image('blog-header','aligncenter'); ?>
		</div>
		<?php
		}
		?>
		<section class="page-content">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div itemprop="articleBody">
							<?php the_content(); ?>
						</div>
					</div>
					<div class="col-md-4">
						<aside class="well">
							<?php get_template_part('entry','meta'); ?>
						</aside>
					</div>
				</div>
				<div class="comments">
					<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}
					?>
				</div>
			</div>
		</section>
	</article>
</main>

<?php
		endwhile;
	endif;
?>

<?php get_footer(); ?>
