// Touch Device Detection
if (Modernizr.touchevents) {
    // Add class to body indicating touch support
    jQuery(document).ready(function() {
        jQuery('body').addClass('touch-support');
    });
} else {
    jQuery(document).ready(function() {
        jQuery("body").addClass('no-touch-support');

        // Swich to hover vs click for dropdowns
        jQuery(function() {
            jQuery('.dropdown').hover(function() {
                    jQuery(this).addClass('open');
                },
                function() {
                    jQuery(this).removeClass('open');
                });
        });

    });
}

// Open/Close Side Nav
jQuery(document).ready(function() {
    var sideslider = jQuery('[data-toggle=collapse-side]');
    var sel = sideslider.attr('data-target');
    var sel2 = sideslider.attr('data-target-2');
    sideslider.click(function(event) {
        jQuery(sel).toggleClass('in');
        jQuery(sel2).toggleClass('out');
    });
});

// Touch Controls
jQuery(document).ready(function() {

    // Side Collapse Menu
    var sideslider = jQuery('[data-toggle=collapse-side]');
    var sel = sideslider.attr('data-target');
    var sel2 = sideslider.attr('data-target-2');

    jQuery("body").swiperight(function(e) {
        if (e.swipestart.coords[0] < 50) {
            jQuery(sel).toggleClass('in');
            jQuery(sel2).toggleClass('out');
        }
    });

    jQuery(".side-collapse").swipeleft(function(e) {
        jQuery(sel).toggleClass('in');
        jQuery(sel2).toggleClass('out');
    });

});


// Floating Labels (Gravity Forms)
(function($) {
    var onClass = "on";
    var showClass = "show";

    jQuery('.ginput_container input, .ginput_container textarea').bind("checkval", function() {
        var label = jQuery(this).parent().prev("label");

        if (this.value !== "") {
            label.addClass(showClass);
        } else {
            label.removeClass(showClass);
        }

    }).on("keyup", function() {

        jQuery(this).trigger("checkval");

    }).on("focus", function() {

        jQuery(this).prev("label").addClass(onClass);

    }).on("blur", function() {

        jQuery(this).prev("label").removeClass(onClass);

    }).trigger("checkval");
})(jQuery);


jQuery(document).ready(function($) {
    /////////////////////////////////////////////
    //Add 'active' class to active checkboxes
    $('.woocommerce form .bld-styled-checkbox input').on('change', function(e) {

        if ($(this).is(':checked')) {
            $(this).parent().addClass('checkbox-active');
        } else {
            $(this).parent().removeClass('checkbox-active');
        }

    });

    /////////////////////////////////////////////
    // Floating labels on WooCommerce forms
    if ($('.woocommerce form').length) {
        var onClass = 'on';
        var showClass = 'show';

        //Floating Label Listener
        $('input:not([type="checkbox"]), textarea, select', $('.woocommerce form')).bind('checkval', function() {

            var parent = $(this).parents('.form-row');

            if (this.value !== '') {
                parent.addClass(showClass);
            } else {
                parent.removeClass(showClass);
            }

        }).on('input', function() {

            $(this).trigger('checkval');

        }).on('focus', function() {

            $(this).parents('.form-row').addClass(onClass);
            $(this).trigger('checkval');

        }).on('blur', function() {

            $(this).parents('.form-row').removeClass(onClass);
            $(this).trigger('checkval');

        }).trigger('checkval');

    }
});

//Sidebar
jQuery(document).ready(function($) {

    $('.sb-quote-tag, .sidebar-button').click(function() {
        $('body').toggleClass('sb-open');

        if ($('body').hasClass('sb-open')) {
            $('body').append('<div class="modal-backdrop fade"></div>');
            $('.modal-backdrop').addClass('in');
        } else {
            $('.modal-backdrop').remove();
        }

    });

    $('.sb-quote-close').click(function() {
        $('body').removeClass('sb-open');
        $('.modal-backdrop').remove();
    });

    $('nav-tabs a').click(function(e) {
        e.preventDefault()
        $(this).tab('show')
    })


});



//Location Scroll Too
jQuery(document).ready(function($) {

    //On location overview link click, scroll to location
    if ($('.scroll-to').length) {
        $('.scroll-to').click(function(e) {
            e.preventDefault();

            $('html, body').animate({
                scrollTop: $(this.hash).offset().top - '150'
            }, 500);
        });
    }

    //Scroll to post Grid when filter paramenters are present
    if ($('.afs-post-grid').length) {
        if (getUrlParameter('type') || getUrlParameter('room') || getUrlParameter('product')) {
            $('html, body').animate({
                scrollTop: $('.afs-post-grid').offset().top - '150'
            }, 750);
        }
    }

});

//Tabbed Content
jQuery(document).ready(function($) {
    if ($('.inspiration-tabs').length) {
        $('.nav-tabs > li:first, .tab-pane:first').addClass('active');
    }
});

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

// MATCH HEIGHTS
jQuery(document).ready(function($) {

    if ($('.promotion-card').length) {

        $('.promotion-card .promotion-card-info-inner').matchHeight({
            byRow: true
        });

        $('.promotion-card .afs-title-md').matchHeight({
            byRow: true
        });
    }


    if ($('#modal-location').length) {
        $('#modal-location .location').matchHeight({
            byRow: true
        });
    }

    if ($('.igtt-card').length) {
        $('.igtt-card .igtt-info').matchHeight({
            byRow: true
        });
    }

    if ($('.afs-about-logo').length) {
        $('.afs-about-logo').matchHeight({
            byRow: true
        });
    }

    if ($('.afs-birthday-radio').length) {
        $('.station-icon').matchHeight({
            byRow: true
        });
    }

});


//Custom Dropdown JS
jQuery(document).ready(function($) {

    var dds = $('.component-filter .dropdown');

    if (dds.length) {

        //Select Filter Functionality
        $('li', dds).click(function() {

            var parent = $(this).parents('.dropdown')[0];

            $('.dropdown-placeholder', parent).text($(this).text());
            $('input', parent)[0].value = $(this).data('value');

            $('.dropdown-clear').addClass('active');

        });

        //Clear Filter Functionality
        $('.dropdown-clear').click(function(e) {

            $('.dropdown > input').val(null);

            $('.dropdown > .dropdown-placeholder').each(function() {
                $(this).text($(this).data('placeholder'));
            });

            $('.dropdown-clear').removeClass('active');

        });

        //Sets Current filter
        dds.each(function(i, el) {
            var selected = $('li.selected', el);
            if (selected.length) {
                $('.dropdown-placeholder', el).text(selected.text());
                $('.dropdown-clear').addClass('active');
            }
        });

    }
});


//AFS Tabs
jQuery(document).ready(function($) {

    if ($('.afs-tabs').length) {
        // tabbed content
        // http://www.entheosweb.com/tutorials/css/tabs.asp
        $(".tab-content").hide();
        $(".tab-content:first").show();

        /* if in tab mode */
        $("ul.tabs li").click(function() {

            $(".tab-content").hide();
            var activeTab = $(this).attr("rel");
            $("#" + activeTab).fadeIn();

            $("ul.tabs li").removeClass("active");
            $(this).addClass("active");

            $(".tab-drawer-heading").removeClass("d-active");
            $(".tab-drawer-heading[rel^='" + activeTab + "']").addClass("d-active");

        });
        /* if in drawer mode */
        $(".tab-drawer-heading").click(function() {

            $(".tab-content").hide();
            var d_activeTab = $(this).attr("rel");
            $("#" + d_activeTab).fadeIn();

            $(".tab-drawer-heading").removeClass("d-active");
            $(this).addClass("d-active");

            $("ul.tabs li").removeClass("active");
            $("ul.tabs li[rel^='" + d_activeTab + "']").addClass("active");
        });


        /* Extra class "tab_last"
           to add border to right side
           of last tab */
        $('ul.tabs li').last().addClass("tab-last");

    }
});

//Page: Inspiration Gallery - LIghtbox
jQuery(document).ready(function($) {

    if ($('.afs-inspiration-grid .igtt-card').length ||
        $('.component-inspirationcta .inspiration-lead').length) {

        lightbox.option({
            'resizeDuration': 0,
            'wrapAround': true,
            'showImageNumberLabel': false,
            'albumLabel': '',
            'fadeDuration': 250,
            'imageFadeDuration': 250,
            'fitImagesInViewport': true,
            'resizeDuration': 250
        })
    }

});

jQuery(document).ready(function($) {
    if ($('.woocommerce-products-header__filter').length) {

        $('.woocommerce-products-header__filter').click(function() {
            $(this).toggleClass('active');
            $('.woocommerce-products-container__sidebar').toggleClass('active');
        })

    }
});

/*
	Coerce the sqft quantity field to meet min/step quanitity on blurr
*/
jQuery(document).ready(function($) {

    if ($('.afs-auto-validate').length) {

        $('.afs-auto-validate').change(function(e) {

            var input = $(this);

            var step = Number(input.attr('step'));
            var min = Number(input.attr('min'));

            var currentValue = Number(input.val());

            //Automatically enforce the min count
            if (currentValue < min) {
                currentValue = min;
                input.val(currentValue);
                $('.product-quantity-notice').show();
                return;
            }

            //Automatically enforce the step count
            if (step > 1) {
                if (currentValue % step !== 0) {
                    currentValue += step - (currentValue % step);
                    input.val(currentValue);
                    $('.product-quantity-notice').show();
                    return;
                }
            }
        })
    }
});

/*
	When a user is purchasing a product that is sold in cartons, we
	update the sqft indicator to show them how many sqft they are purchasing
*/
jQuery(document).ready(function($) {

    if ($('.afs-auto-sqft').length) {

        $('.afs-auto-sqft').on('input', function(e) {

            var input = $(this);
            var sqftpc = Number(input.data('sqftpc'));
            var currentValue = Number(input.val());
            var sqft = (currentValue * sqftpc).toFixed(2);

            //Update sqft counter
            $('.product-sqft-notice .quantity').text(sqft);
        })
    }
});

jQuery(document).ready(function($) {

    $(".register .g-recaptcha").insertAfter(".register #billing_company_field");
});