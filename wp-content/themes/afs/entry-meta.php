<div class="entry-meta">

	<span class="sr-only" itemprop="url"><?php the_permalink(); ?></span>
	<span class="sr-only" itemprop="publisher"><?php bloginfo('name'); ?></span>
	<span class="sr-only" itemprop="headline"><?php the_title(); ?></span>

	<?php echo get_avatar( get_the_author_meta( 'ID' ), 120, $default, $alt, array('class'=>'alignright img-circle bg-white') ); ?>

	<ul class="fa-ul">

		<li>
			<i class="fa-li fa fa-user"></i>
			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>">
				<span itemprop="author" itemscope itemtype="http://schema.org/Person">
					<span itemprop="name">
						<?php the_author(); ?>
					</span>
				</span>
			</a>
		</li>

		<li>
			<i class="fa-li fa fa-calendar"></i>
			<?php
				echo '<time class="published" datetime="'.get_the_time('c').'" itemprop="datePublished" content="'.get_the_time("Y-m-d", $post->ID).'">'
					.get_the_time("D. M d, Y", $post->ID)
					.'</time>';
			?>
		</li>

		<?php
		if (get_the_category_list() != '') {
		?>
		<li>
			<i class="fa-li fa fa-folder"></i>
			<?php echo get_the_category_list( ', ' ); ?>
		</li>
		<?php } ?>

		<?php
		if (get_the_tag_list() != '') {
		?>
		<li>
			<i class="fa-li fa fa-tags"></i>
			<?php echo get_the_tag_list( '', ', ' ); ?>
		</li>
		<?php } ?>

	</ul>



</div>
