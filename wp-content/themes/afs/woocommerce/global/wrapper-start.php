<?php
/**
 * Content wrappers
 *
 * Modifications
 * - Simplify
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<div id="primary" class="content-area">
	<main id="main" class="site-main container" role="main">
