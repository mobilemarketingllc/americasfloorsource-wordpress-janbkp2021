<?php
	$promo_image = get_field('promo_image');
	$promo_links = get_field('promo_links');

		$tax_type = ($post_type == 'inspirations') ? array('room_cat','product_type') : 'tt_cat';

	$card_categories = wp_get_post_terms(
		get_the_ID(),
		'product_type',
		array(
			'orderby' => 'taxonomy',
			'order' => 'ASC'
		)
	);

?>

	<div class="promotion-card <?php echo ($promo_links) ? 'promotion-card-hover ' : '' ; ?>col-lg-3 col-md-4 col-sm-4 col-xs-6">
		<div class="promotion-card-image">
			<img src="<?php echo $promo_image['sizes']['split-cta-md']; ?>" alt="<?php echo $promo_image['alt']; ?>">
			<?php if ($promo_links): ?>
				<div class="promotion-card-action">
					<?php foreach ($promo_links as $key => $link): ?>
						<a class="btn--rounded" href="<?php echo $link['link_url'] ?>"><?php echo $link['link_label'] ?></a>
					<?php endforeach ?>
				</div>
			<?php endif ?>

			<?php if ($card_categories): ?>
				<?php echo display_inline_cat($card_categories, 'inline-category-blue', get_bloginfo('url') . '/inspiration-gallery/', 'type'); ?>
			<?php endif ?>
		</div>
		<div class="promotion-card-info">
			<div class="promotion-card-info-inner">
				<h4 class="afs-title-md"><?php the_title(); ?></h4>
				<?php echo get_field("promo_content"); ?>
			</div>
		</div>
	</div>
