<?php

	$post_type = get_post_type();

	$tax_type = ($post_type == 'inspirations') ? array('room_cat','product_type') : 'tt_cat';

	$card_categories = wp_get_post_terms(
		get_the_ID(),
		$tax_type,
		array(
			'orderby' => 'taxonomy',
			'order' => 'ASC'
		)
	);

	$card_image = get_field("header_bg") ?: get_field("placeholder_image", "option");

?>

<div class="igtt-card col-lg-3 col-md-4 col-sm-4 col-xs-6">
		<a href="<?php the_permalink(); ?>">
			<img src="<?php echo $card_image['sizes']['hero-xxs']; ?>" alt="<?php echo $card_image['alt']; ?>">
		</a>
		<div class="igtt-info">
			<?php echo display_inline_cat($card_categories, 'inline-category', $base_url, 'type'); ?>
			<h3 class="igtt-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		</div>
</div>


