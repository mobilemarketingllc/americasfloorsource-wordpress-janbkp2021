<!-- Section: No Query Results -->
<section class="afs-section afs-post-grid component-no-grid">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="afs-title-lg">Whoops!</h1>
				<p class="afs-body-main">
					Looks like there are no results from your search parameters.</br>
					Use the filters to change your search or <a href="<?php the_permalink(); ?>">View All</a>.</br>
				</p>
			</div>
		</div>
	</div>
</section>
