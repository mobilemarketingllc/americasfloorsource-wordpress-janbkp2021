<?php
	$testimonial_bg['component-testimonial'] = get_field('testimonial_bg', 'option');


	//Component: Product Bar
	$args = array(
		'post_type' => 'testimonial',
		'posts_per_page' => 1,
		'orderby' => 'rand'

	);

	$args['category_name'] = (isset($testimonial_cat)) ? $testimonial_cat : 'b2c';

	$testimonial = new WP_Query($args);

	if($testimonial->have_posts()) :
		$testimonial->the_post();
?>
		<section class="afs-section component-testimonial">
			<?php
				echo responsive_backgrounds_skinny ($testimonial_bg);
			?>

			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</section>

<?php
	endif;

	wp_reset_postdata()
?>