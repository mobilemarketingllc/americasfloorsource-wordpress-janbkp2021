<?php
	$financingcta_bg['component-fincta'] = get_field('financing_bg_image', 'option');
?>

<section class="afs-section component-fincta">
	<?php echo responsive_backgrounds_skinny($financingcta_bg) ?>
	<div class="container">
		<div class="row">
			<div class="cta-content col-md-10 col-sm-9">
				<?php echo get_field('financing_content', 'option')?>
			</div>
			<div class="cta-btn col-md-2 col-sm-3">
				<a class="btn--rounded btn--nocaret" href="<?php echo get_field('financing_link', 'option')?>"><?php echo get_field('financing_btn_label', 'option')?></a>
			</div>
		</div>
	</div>
</section>