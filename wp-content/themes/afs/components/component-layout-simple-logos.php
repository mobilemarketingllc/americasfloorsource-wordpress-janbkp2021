<div class="afs-about-simple afs-CAS-1 col-lg-9 col-md-9">
	<?php echo $section['simple_content']; ?>
</div>


<?php if ($section['simple_logos']): ?>
	<div class="afs-about-logos col-lg-9 col-md-9">
		<?php foreach ($section['simple_logos'] as $key => $logo): ?>

			<div class="afs-about-logo col-lg-4 col-md-6 col-xs-6">
				<a href="<?php echo $logo['item_url']; ?>" target="_blank">
					<img src="<?php echo $logo['item_image']['sizes']['promo-xxs']; ?>" alt="<?php echo $logo['item_image']['alt']; ?>">
					<h2 class="icta-subtitle"><?php echo $logo['item_title']; ?></h2>
				</a>
			</div>

		<?php endforeach ?>
	</div>
<?php endif ?>

