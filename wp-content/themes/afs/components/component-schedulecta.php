<?php
	$schedule_bg['component-schedulecta'] = get_field('smcta_sched_bg_image', 'option');

	$schedule_btn_img = get_field('smcta_sched_btn_image', 'option');

?>

<section class="afs-section component-schedulecta">
	<?php echo responsive_backgrounds_skinny($schedule_bg) ?>
	<div class="container">
		<div class="row">
			<div class="cta-content col-sm-7">
				<?php echo get_field('smcta_sched_content', 'option')?>
			</div>
			<div class="cta-btn col-sm-5">
				<a class="btn--rounded btn--nocaret" href="<?php echo get_field('smcta_sched_link', 'option')?>"><?php echo get_field('smcta_sched_btn_label', 'option')?></a>
				<img src="<?php echo $schedule_btn_img['url'] ?>" alt="<?php echo $schedule_btn_img['alt'] ?>">
			</div>
		</div>
	</div>
</section>