<?php if ($section['locations_enabled']):
	$locations_array = get_all_locations(); ?>

	<div class="afs-about-location afs-CAS-1 col-lg-9 col-md-9">
		<h2 class="afs-title-lg">Our Locations</h2>

		<?php foreach ($locations_array as $key => $location):

			$phone = get_field("location_phone", $location->ID);

		?>

			<div class="location location-mini">
				<h3 class="afs-title-md"><?php the_field("location_shortname", $location->ID) ?></h3>
				<div class="location-info">
					<address>
						<a class="location-address" href="<?php echo get_field("location_gmap_link", $location->ID ); ?>">
							<?php echo format_address( $location->ID ); ?>
						</a>
					</address>

					<a class="location-phone" href="tel:+1<?php echo $phone; ?>"><?php echo format_phone( $phone ); ?></a>
				</div>

			</div>

		<?php endforeach ?>

	</div>

<?php endif ?>