<?php
	$backgrounds['afs-sp-wholesale-2018'] = get_sub_field('background_image');
	$image = get_sub_field('foreground_image');
?>

<!-- Section: Enter to Win -->
<section class="afs-section afs-sp-wholesale-2018">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-6 col-md-8 col-md-offset-2 content">
				<img src="<?php echo $image['url'] ?> " alt="<?php echo $image['alt'] ?>">

				<div class="content__info">

					<div class="content__location">
						<h3>Location</h3>

						<p>
							America's Floor Source<br/>
							Columbus East
						</p>

						<a href="">
							<address>
								3442 Millennium Court,<br/>
								Columbus, Ohio 43219<br/>
								Citygate Business Park
							</address>
						</a>

						<a href="" class="btn--square">Get Directions</a>

					</div>

					<div class="content__hours">
						<h3>Hours</h3>

						<p>
							Friday, January 5th<br/>
							11:00am - 8:00pm
						</p>

						<p>
							Saturday, January 6th<br/>
							9:00am - 8:00pm
						</p>

						<p>
							Sunday, January 7th<br/>
							10:00am - 6:00pm
						</p>

					</div>


				</div>
			</div>
		</div>
	</div>

	<?php echo responsive_backgrounds_full ($backgrounds) ?>
</section>