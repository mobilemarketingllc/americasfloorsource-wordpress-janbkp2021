<div class="afs-section component-sidebarcta col-lg-3">

	<?php foreach ($sidebar_content as $key => $cta):
		// print_r($cta);
	?>

		<div class="cta">
			<img src="<?php echo $cta['item_image']['url']; ?>" alt="<?php echo $cta['item_image']['alt'] ?>">
			<a class="btn--rounded" href="<?php echo $cta['item_btn_url']; ?>"><?php echo $cta['item_btn_label']; ?></a>
		</div>

	<?php endforeach ?>

</div>
