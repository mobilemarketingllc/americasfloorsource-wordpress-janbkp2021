<div class="text"><?php echo $value['tat_icons_text']; ?></div>


<?php if ($value['tat_icons_icons']): ?>
	<div class="icons-section">
		<?php foreach ($value['tat_icons_icons'] as $key => $section): ?>

			<div class="icon">
				<img src="<?php echo $section['item_icon']['url'] ?>" alt="<?php echo $section['item_icon']['alt'] ?>">
				<div class="icons-section-text">
					<?php echo $section['item_text']; ?>
				</div>
			</div>

		<?php endforeach ?>
	</div>
<?php endif ?>