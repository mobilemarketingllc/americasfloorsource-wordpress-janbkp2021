<?php
	//Categories set to display
	$inspiration_cats_room = get_field('inspiration_cat_room');
	$inspiration_cats_product = get_field('inspiration_cat_product');
	$inspiration_cats_behavior = get_field('inspiration_cat_behavior');


	// print_r($inspiration_cats);
	$base_url = get_the_permalink();

	$args = array(
		'post_type' => 'inspirations',
		'posts_per_page' => 3,
		'order' => 'DESC',
		'orderby' => 'date'
	);

	//Add categories to display to the query
	if(!empty($inspiration_cats_room) || !empty($inspiration_cats_product)) {
		$args['tax_query'] = array(
			'relation' => $inspiration_cats_behavior
		);

		if(!empty($inspiration_cats_room)) {

			$args['tax_query'][] = array(
				'taxonomy' => 'room_cat',
				'field' => 'term_taxonomy_id',
				'terms' => $inspiration_cats_room
			);
		}

		if(!empty($inspiration_cats_product)) {
			$args['tax_query'][] = array(
				'taxonomy' => 'product_type',
				'field' => 'term_taxonomy_id',
				'terms' => $inspiration_cats_product
			);
		}
	}

	$inspirations = new WP_Query($args);

	// Filtering Mechanism
	// Get all terms
	$terms = get_terms(array(
		'taxonomy' => array('room_cat', 'product_type'),
		'orderby' => 'order_menu',
		'order' => 'ASC',
		'hide_empty' => 0
	));
	// print_r($terms);

	$terms_filtered = array();
	foreach ($terms as $key => $t) {
		$terms_filtered[$t->taxonomy][] = $t;
	}

?>

<section class="afs-section component-inspirationcta">
	<div class="container">
		<div class="row">
			<div class="inspiration-static
						col-lg-3 col-lg-offset-0
						col-sm-8 col-sm-offset-2">
				<?php echo get_field('inspiration_content', 'option')?>
			</div>

			<?php
				if($inspirations->have_posts()) :

					while($inspirations->have_posts()) :
						$inspirations->the_post();

						$image = get_field('header_bg');
						$card_categories = wp_get_post_terms(
							get_the_ID(),
							array(
								'room_cat',
								'product_type'
							),
							array(
								'orderby' => 'taxonomy',
								'order' => 'ASC'
							)
						);

						$product_title = (get_field("inspiration_product")) ? ' data-title="' . esc_html(get_field("inspiration_product")) . '"' : '' ;
			?>

						<div class="inspiration-lead
									col-lg-3
									col-sm-4
									col-xs-4">
							<a href="<?php echo $image['sizes']['hero-lg'] ?>" data-lightbox="Inspiration Gallery" <?php echo $product_title; ?>>
								<img src="<?php echo $image['sizes']['hero-xxs'] ?>" alt="<?php echo $image['alt'] ?>">
							</a>
							<?php echo display_inline_cat($card_categories, 'inline-category-blue', get_bloginfo('url') . '/inspiration-gallery/', 'type'); ?>
						</div>

			<?php
					endwhile;
				endif;

				wp_reset_postdata()
			?>

			<div class="inspiration-actions
						col-lg-3
						col-md-12
						col-sm-12
						col-xs-12">
				<div class="inspiration-actions-link">
					<a class="btn--arrow" href="<?php bloginfo("url") ?>/inspiration-gallery/">View our Inspiration Gallery</a>
				</div>

				<div class="inspiration-actions-filters">
					<div class="dropdown">
						<span class="dropdown-placeholder">Browse by Material</span>
						<ul class="dropdown-menu">
							<?php foreach ($terms_filtered['product_type'] as $key => $value): ?>
								<li><a href="<?php echo get_bloginfo("url") . '/inspiration-gallery/?type=' . $value->slug; ?>"><?php echo $value->name; ?></a></li>
							<?php endforeach ?>
						</ul>
					</div>

					<div class="dropdown">
						<span class="dropdown-placeholder">Browse by Room</span>
						<ul class="dropdown-menu">
							<?php foreach ($terms_filtered['room_cat'] as $key => $value): ?>
								<li><a href="<?php echo get_bloginfo("url") . '/inspiration-gallery/?type=' . $value->slug; ?>"><?php echo $value->name; ?></a></li>
							<?php endforeach ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
