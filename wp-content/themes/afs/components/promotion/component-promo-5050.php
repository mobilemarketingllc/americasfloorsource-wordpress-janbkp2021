<?php

	$cols = get_sub_field('3col_columns');
	$style = get_sub_field('3col_style');

?>

<!-- Section: Enter to Win -->
<section class="afs-section afs-sp afs-sp-5050">
	<div class="container">
		<div class="col-md-6">
			<?php the_sub_field('column_1') ?>
		</div>
		<div class="col-md-6">
			<?php the_sub_field('column_2') ?>
		</div>
	</div>
</section>


