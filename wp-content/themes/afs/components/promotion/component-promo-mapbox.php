<?php

	$bg['afs-sp-mapbox'] = get_sub_field('map_bg');

?>


<!-- Section: Enter to Win -->
<section class="afs-section afs-sp-mapbox">

	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<?php the_sub_field('map_content'); ?>
			</div>
		</div>
	</div>

	<?php echo responsive_backgrounds_full ($bg) ?>
</section>