<?php

	$cols = get_sub_field('3col_columns');
	$style = get_sub_field('3col_style');

?>


<!-- Section: Enter to Win -->
<section class="afs-section afs-sp-3col <?php echo $style;?>">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="afs-title-lg"><?php the_sub_field('3col_title'); ?></h3>
			</div>

			<?php foreach ($cols as $key => $col): ?>

				<div class="item col-md-4">
					<img class="item-icon" src="<?php echo $col['col_icon']['url']; ?>" alt="<?php echo $col['col_icon']['alt']; ?>">
					<div class="item-content">
						<?php echo $col['col_content'] ?>
					</div>
				</div>

			<?php endforeach ?>
		</div>
	</div>
</section>