<?php

	$bg['afs-sp-simple'] = get_sub_field('simple_background');

?>


<!-- Section: Enter to Win -->
<section class="afs-section afs-sp-simple afs-sp-simple-bg">

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<?php the_sub_field('simple_content'); ?>
			</div>
		</div>
	</div>

	<?php echo responsive_backgrounds_full ($bg) ?>
</section>