<?php
	$cols = get_sub_field('5col_columns');
?>

<!-- Section: Enter to Win -->
<section class="afs-section afs-sp-5col">
	<div class="container">
			<div class="afs-sp-5col__title">
				<h3 class="afs-title-alt"><?php the_sub_field('5col_title'); ?></h3>
			</div>

			<div class="afs-sp-5col__columns">

				<?php foreach ($cols as $key => $col): ?>

					<div class="afs-sp-5col__column">
						<img class="col-icon" src="<?php echo $col['col_icon']['url']; ?>" alt="<?php echo $col['col_icon']['alt']; ?>">
						<div class="col-content">
							<?php echo $col['col_content'] ?>
						</div>
					</div>

				<?php endforeach ?>

			</div>



	</div>
</section>