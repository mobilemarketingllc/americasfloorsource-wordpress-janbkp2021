<?php

	$logos = get_sub_field('sl_logos');

?>


<!-- Section: Enter to Win -->
<section class="afs-section afs-sp-logos">
	<div class="container">
		<div class="row">
			<div class="afs-sp-logos-content col-lg-12">
				<?php the_sub_field('sl_content'); ?>
			</div>

			<div class="afs-sp-logos-logos col-lg-12">

				<?php foreach ($logos as $key => $logo): ?>

					<div class="logo">
						<img class="logo-icon" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>">
					</div>

				<?php endforeach ?>

			</div>

		</div>
	</div>
</section>