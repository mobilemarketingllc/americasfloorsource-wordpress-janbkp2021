<?php
	$backgrounds['afs-sp-form'] = get_sub_field('contest_bg');
	$style = get_sub_field('contest_style');
?>

	<!-- Section: Enter to Win -->
	<section class="afs-section afs-sp-form <?php echo $style ?>">
		<div class="container">
			<div class="row">
				<div class="form-content
							col-lg-6 col-lg-offset-6
							col-md-8 col-md-offset-4
							col-sm-10 col-sm-offset-2 col-xs-12">
					<div class="form-container">

						<?php if ($style == 'birthday'): ?>

							<div class="form-header">
								<div class="birthday-l1">
									<p class="birthday-l1-main"><?php the_sub_field('contest_header_1'); ?></p>
								</div>
								<div class="birthday-l2"><?php the_sub_field('contest_header_2'); ?></div>
							</div>

						<?php elseif ($style == 'parade'): ?>

							<div class="form-header">
								<p class="parade-l1"><?php the_sub_field('contest_header_1'); ?></p>
								<p class="parade-l2"><?php the_sub_field('contest_header_2'); ?></p>
								<p class="parade-l3"><?php the_sub_field('contest_header_3'); ?></p>
								<p class="parade-l4"><?php the_sub_field('contest_header_4'); ?></p>
							</div>

						<?php endif; ?>


						<div class="form-form">
							<?php the_sub_field('contest_form'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php echo responsive_backgrounds_full ($backgrounds) ?>
	</section>