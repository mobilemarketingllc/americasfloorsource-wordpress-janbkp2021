<?php


	$now = date("Y-m-d H:i:s", current_time('timestamp'));

	//Filter for Active Promotions
	$args = array(
		'post_type' => 'promotion',
		'posts_per_page' => -1,
		'order' => 'DESC',
		'orderby' => 'date',
		'tax_query' => array(
			array(
				'taxonomy' => 'category',
				'field'    => 'slug',
				'terms'    => 'birthday-sale'
			),
		)
	);

	$promotions = new WP_Query($args);

?>

<?php if ($promotions->have_posts()): ?>

	<!-- Section: Inspiration Grid -->
	<section class="afs-section afs-post-grid afs-promotions-grid afs-sp-promotions">

		<div class="container">
			<div class="row">

				<div class="afs-post-grid-header afs-CAS-1 col-sm-12">
					<?php the_sub_field("promo_content"); ?>
				</div>

			</div>
			<div class="row afs-sp-promotions-container">
				<?php
					while($promotions->have_posts()) :
						$promotions->the_post();

						$promo_image = get_field('promo_image');

				?>
					<div class="promotion-card col-lg-3 col-md-3 col-sm-4 col-xs-6">
							<div class="promotion-card-image">
							<img src="<?php echo $promo_image['sizes']['split-cta-md']; ?>" alt="<?php echo $promo_image['alt']; ?>">
						</div>
						<div class="promotion-card-info">
							<div class="promotion-card-info-inner">
								<h4 class="afs-title-md"><?php the_title(); ?></h4>
								<?php echo get_field("promo_content"); ?>
							</div>
						</div>
					</div>

				<?php
					endwhile;
				?>
			</div>
		</div>
	</section>

<?php
	else :
		//No posts returned
		include('components/component-no-grid.php');

	endif;

	wp_reset_postdata()
?>