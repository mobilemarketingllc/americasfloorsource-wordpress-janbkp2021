<!-- Section: Simple Content -->
<section class="afs-section afs-sp afs-sp-simple">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<?php the_sub_field('simple_content'); ?>
			</div>
		</div>
	</div>
</section>