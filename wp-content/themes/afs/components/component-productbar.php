<?php
	//Component: Product Bar
	$args = array(
		'post_type' => 'page',
		'post_parent' => 131,
		'post__not_in' => array(2482),
		'order' => 'ASC',
		'orderby' => 'menu_order'
	);

	$products = new WP_Query($args);

	if($products->have_posts()) :

?>

		<section class="afs-section component-productbar">

			<div class="component-graytitle">
				<div class="graytitle-text">
					<?php echo get_field('productbar_title', 'option')?>
				</div>
			</div>

			<div class="productbar-container">
				<?php
					while($products->have_posts()) :

						$products->the_post();
				?>
								<div class="product" style="background-image: url('<?php echo get_field('product_bar_image')['sizes']['thumbnail'] ?>')">
									<a class="product-title" href="<?php the_permalink(); ?>">
										<?php the_title(); ?>
									</a>
								</div>

				<?php
					endwhile;
				?>
			</div>
		</section>

<?php
	endif;

	wp_reset_postdata()
?>