<?php
	if(is_archive()) {
		$header_title = str_replace("Archives: ", "", get_the_archive_title());
	} else {
		$header_title = (get_field('header_title')) ? get_field('header_title') : get_the_title();
	}

	$header_title_style = (wp_get_post_parent_id(get_the_ID()) != 0) ? ' header-title-small' : '';

	$skinny_bg['header-skinny'] = get_field("header_bg");
?>

	<section class="afs-section header-skinny">
		<?php echo responsive_backgrounds_skinny ($skinny_bg) ?>
		<div class="container">
			<div class="row">
				<div class="header-title<?php echo $header_title_style; ?>">
					<?php echo $header_title; ?>
				</div>
			</div>
		</div>
	</section>
