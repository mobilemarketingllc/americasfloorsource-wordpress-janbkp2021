<?php
	get_header();

	$c3_col_1 = get_field("about_c3_col_1");
	$c3_col_2 = get_field("about_c3_col_2");
	$c3_col_3 = get_field("about_c3_col_3");

	$sidebar_content = get_field("cta_sidebar");
?>

<main id="main" role="main">

	<!-- Section: Header Large -->
	<?php include('components/header-skinny.php'); ?>

	<!-- Section: Content with Sidebar -->
	<section class="afs-section afs-about">
		<div class="container">
			<div class="row">
				<div class="afs-about-main afs-CAS-1 col-lg-9">
					<?php the_field("about_content"); ?>
				</div>

				<?php
					if ($sidebar_content) {
						include('components/component-sidebarcta.php');
					}
				?>
			</div>
			<div class="row">
				<div class="afs-about-c3 afs-CAS-1 col-lg-9">
					<?php the_field("about_c3_header"); ?>

					<?php if ($c3_col_1): ?>
						<div class="col-lg-4">
							<?php echo $c3_col_1; ?>
						</div>
					<?php endif ?>

					<?php if ($c3_col_2): ?>
						<div class="col-lg-4">
							<?php echo $c3_col_2; ?>
						</div>
					<?php endif ?>

					<?php if ($c3_col_3): ?>
						<div class="col-lg-4">
							<?php echo $c3_col_3; ?>
						</div>
					<?php endif ?>

				</div>

			</div>
		</div>
	</section>

	<!-- Section: Testimonial -->
	<?php include('components/component-testimonial.php'); ?>

</main>

<?php

	get_footer();

?>
