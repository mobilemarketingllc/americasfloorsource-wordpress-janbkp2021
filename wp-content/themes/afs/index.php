<?php get_header(); ?>

<header class="page-header">
	<div class="container">
		<h1 class="heading"><?php echo get_the_archive_title(); ?></h1>
	</div>
</header>

<div class="container">
	<div class="row">

		<main id="main" role="main" class="col-sm-8 col-xs-12 post-loop">

			<div class="row">

				<?php
					if ( have_posts() ) :
						while ( have_posts() ) : the_post();
				?>

				<div class="col-xs-12">

				<article id="post-<?php the_id(); ?>" class="post clearfix">
					<header class="post-header">
						<h2 class="heading">
							<a href="<?php the_permalink(); ?>" title="Read <?php the_title(); ?>">
								<?php the_title(); ?>
							</a>
						</h2>
						<?php get_template_part('entry','meta-loop'); ?>
					</header>
					<section class="post-content">
						<a href="<?php the_permalink(); ?>" title="Read <?php the_title(); ?>">
						<?php
						if ( has_post_thumbnail() ) {
							the_post_thumbnail('thumbnail', array( 'class' => 'alignright' ));
						}
						?>
						</a>
						<?php echo get_the_excerpt(); ?>
					</section>
				</article>

				</div>

				<?php
						endwhile;
					endif;
				?>

			</div>

			<div class="row">
				<div class="col-xs-12">
					<div class="paginate">
						<?php default_paginate(); ?>
					</div>
				</div>
			</div>

		</main>

		<aside id="sidebar" class="col-sm-4 col-xs-12">
			<?php get_sidebar(); ?>
		</aside>

	</div>
</div>

<?php get_footer(); ?>
