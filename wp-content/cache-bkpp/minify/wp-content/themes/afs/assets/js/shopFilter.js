(function(){let filterMap={default:["product_cat","pa_brand","pa_color-family","mta_price_per_sqft_0_23"]}
if(archiveFilterMap){filterMap=archiveFilterMap;}
let pathName=window.location.pathname;let isShop=pathName.includes('/shop/');let isCategory=pathName.includes('/product_cat/');if(isShop){updateFilters(['default']);}
if(isCategory){let pieces=pathName.split('/').filter(function(item){return item!=="";});let categories=pieces[1].split(',');updateFilters(categories);}
function updateFilters(filters){let activeFilters=[];filters.forEach(function(key){activeFilters=activeFilters.concat(filterMap[key])})
activeFilters=activeFilters.filter(function(value,index,self){return self.indexOf(value)===index;})
let allFilters=document.querySelectorAll('div.prdctfltr_filter');allFilters.forEach(function(filter){let filterName=filter.dataset.filter;let isActive=filter.querySelectorAll('label.prdctfltr_active').length;if(activeFilters.includes(filterName)||isActive){filter.classList.add('show');}else{filter.classList.remove('show');filter.classList.add('hide');}});}})()