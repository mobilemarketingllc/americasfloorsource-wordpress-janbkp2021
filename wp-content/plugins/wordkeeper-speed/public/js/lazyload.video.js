var wordkeeper_video = {};

(function ($) {
	$((function() {

	  var videos = $('.wordkeeper-video');
	  $.each(videos, function(index, item){
			item = $(item);

			if(item.css('float') == 'left' || item.css('float') == 'right') {
				if(item.prop('style')[jQuery.camelCase('maxWidth')] != '') {
					item.css('width', item.prop('style')[jQuery.camelCase('maxWidth')]);
				}
				else if(item.prop('style')[jQuery.camelCase('minWidth')] != '') {
					item.css('width', item.prop('style')[jQuery.camelCase('minWidth')]);
				}
			}

			// Adjust the display window and compatibility with WP Bakery Video Player
			var width = item.outerWidth();
			var height = 0.5625 * width;
			var parent = item.parent();
			if(parent.has('wpb_video_wrapper')) {
				parent.css('paddingTop', '0px');
			}

			item.css('height', height + 'px');
	  });

	  var youtube = $('[data-type="youtube"]');
	  $.each(youtube, function(index, item){
			item = $(item);

			var source = 'https://img.youtube.com/vi/' + item.data('embed') + '/hqdefault.jpg';
			item.css('background', 'url("' + source + '") no-repeat center center');
			item.css('background-size', 'cover');

			item.click(function(e) {
			  var iframe = document.createElement('iframe');

			  iframe.setAttribute('frameborder', '0');
			  iframe.setAttribute('allowfullscreen', '');
			  iframe.setAttribute('src', 'https://www.youtube.com/embed/' + this.dataset.embed + '?rel=0&showinfo=0&autoplay=1');

			  this.innerHTML = '';
			  this.appendChild(iframe);
			});
	  });

	  var vimeo = $('[data-type="vimeo"]');
	  $.each(vimeo, function(index, item){
		item = $(item);

	    $.ajax({
		    url: 'https://vimeo.com/api/v2/video/' + item.data('embed') + '.json',
		    dataType: 'json',
		    success: function(data) {
			  	var source = data[0].thumbnail_large;
				item.css('background', 'url("' + source + '") no-repeat center center');
				item.css('background-size', 'cover');
		    }
	    });

		item.click(function(e) {
		  var iframe = document.createElement('iframe');

		  iframe.setAttribute('frameborder', '0');
		  iframe.setAttribute('allowfullscreen', '');
		  iframe.setAttribute('src', 'https://player.vimeo.com/video/' + this.dataset.embed + '?color=949494&title=0&byline=0&portrait=0&autoplay=1');

		  this.innerHTML = '';
		  this.appendChild(iframe);
		});
	  });

	  var wistia = $('[data-type="wistia"]');
	  $.each(wistia, function(index, item){
	  	item = $(item);

	    $.ajax({
		    url: 'https://fast.wistia.com/oembed?url=' + encodeURI('https://fast.wistia.net/embed/iframe/' + item.data('embed')),
		    dataType: 'json',
		    success: function(data) {
			  	var source = data.thumbnail_url;
				item.css('background', 'url("' + source + '") no-repeat center center');
				item.css('background-size', 'cover');
		    }
	    });

		item.click(function(e) {
		  var iframe = document.createElement('iframe');

		  iframe.setAttribute('frameborder', '0');
		  iframe.setAttribute('allowfullscreen', '');
		  iframe.setAttribute('src', '//fast.wistia.net/embed/iframe/' + this.dataset.embed + '?autoplay=1');

		  this.innerHTML = '';
		  this.appendChild(iframe);
		});
	  });
	}));
})(jQuery);
