<?php

/**
 * This class provides helper methods
 *
 * @package:     WordKeeper Speed Helpers
 * @author:  WordKeeper
 * @Author URI: http://www.wordkeeper.com
 *
 */
class WordKeeper_Speed_Helpers {

	/**
	 * Checks if the passed url is internal or external
	 *
	 * @access static
	 * @param string $url
	 * @return bool
	 */
	public static function is_external_url($url) {
		$site_url = self::get_site_url_without_protocol();
		$site_url_dot = '.' . $site_url;
		$components = parse_url($url);
		if (empty($components['host'])) return false;  // we will treat url like '/relative.php' as relative
		if (strcasecmp($components['host'], $site_url) === 0) return false; // url host looks exactly like the local host
		return strrpos(strtolower($components['host']), $site_url_dot) !== strlen($components['host']) - strlen($site_url_dot); // check if the url host is a subdomain
	}

	/**
	 * Returns the url without http:// or https://
	 *
	 * @access static
	 * @return string
	 */
	public static function get_site_url_without_protocol() {
		$url = get_site_url();
		$url_parts = parse_url($url);
		$url = $url_parts['host'];
		return $url;
	}

	/**
	 * Writes the debug output to a file
	 *
	 * @access static
	 * @param mixed $str
	 * @param bool $is_array
	 * @return void
	 */
	public static function debug_file($str, $is_array = false) {
		$file = ABSPATH . 'wp-content/cache/test.txt';
		if($is_array){
			$json_string = json_encode($str, JSON_PRETTY_PRINT);
			file_put_contents( $file, $json_string . PHP_EOL, FILE_APPEND );
			return;
		}

		file_put_contents( $file, $str . PHP_EOL, FILE_APPEND );
	}

	/**
	 * Checks if the front end optimization is disabled for the site
	 *
	 * @access static
	 * @param array $settings
	 * @return bool
	 */
	public static function is_optimization_disabled($settings){

		if($settings['wordkeeper']['disable-optimization-for-editors'] == true && is_user_logged_in()){
			return true;
		}

		$hard_override_plugin_keywords = array('themify');
		$active_plugins = get_option('active_plugins');
		$found = false;

		foreach($hard_override_plugin_keywords as $kw){
			if(!in_array($kw, $active_plugins)){
				continue;
			}
			$found = true;
		}

		if( current_user_can('edit_pages') || current_user_can('edit_posts') ) {
			$found = true;
		}

		return $found && is_user_logged_in();
	}

	/**
	 * Checks if the optimization is disabled for editors
	 *
	 * @access static
	 * @return bool
	 */
	public static function is_optimization_disabled_for_editors(){

		if( current_user_can('edit_pages') || current_user_can('edit_posts') ) {
			return true;
		}
		return false;
	}


	/**
	 * purge_minify_cache function.
	 *
	 * @access public
	 * @static
	 * @return void
	 */
	public static function purge_minify_cache(){
		$cache_root = '';
		if(is_multisite()) {
			$cache_root = ABSPATH . 'wp-content/cache/minify/' . get_current_blog_id();
		}
		else {
			$cache_root = ABSPATH . 'wp-content/cache/minify';
		}
		if(is_dir($cache_root)){
			self::recursive_remove_directory($cache_root);
		}
	}


	/**
	 * recursive_remove_directory function.
	 *
	 * @access public
	 * @static
	 * @param mixed $directory
	 * @return void
	 */
	public static function recursive_remove_directory($directory){
		foreach(glob("{$directory}/*") as $file){
			if(is_dir($file)){
				self::recursive_remove_directory($file);
			} else {
				unlink($file);
			}
		}
		rmdir($directory);
	}
}
