<?php

/**
 * Fired during plugin activation
 *
 * @link       https://wordkeeper.com/
 * @since      1.0.0
 *
 * @package    Wordkeeper_Speed
 * @subpackage Wordkeeper_Speed/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wordkeeper_Speed
 * @subpackage Wordkeeper_Speed/includes
 * @author     Lance Dockins <info@wordkeeper.com>
 */
class Wordkeeper_Speed_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		if(Wordkeeper_Speed_Config::get_instance()->site_config_exists() == false){
			Wordkeeper_Speed_Config::get_instance()->init();
			Wordkeeper_Speed_Config::get_instance()->create_default_site_config();
		}
		else{
			Wordkeeper_Speed_Config::get_instance()->init();
			$requires_update = Wordkeeper_Speed_Config::get_instance()->requires_schema_update();
			if($requires_update){
				Wordkeeper_Speed_Config::get_instance()->upgrade_site_config();
			}
		}
	}

}
