<?php

/**
 * This class handles the file system based cache management for widgets
 *
 * @package:     WordKeeper Speed Widget Cache
 * @author:  WordKeeper
 * @Author URI: http://www.wordkeeper.com
 *
 */
class WordKeeper_Speed_Widget_Cache {

	public static $cache_path = ABSPATH . 'wp-content/cache/partials/widget/';

	/**
	 * Returns the widget cache path
	 *
	 * @access static
	 * @param void
	 * @return mixed
	 */
	public static function get_cache_path(){
		if ( is_multisite() ){
			return ABSPATH . 'wp-content/cache/' . get_current_blog_id() . '/partials/widget/';
		}
		return ABSPATH . 'wp-content/cache/partials/widget/';
	}

	/**
	 * Generates and returns hashed key
	 *
	 * @access static
	 * @param object $args
	 * @return mixed
	 */
	public static function generate_cache_key($instance,$widget, $url_specific = false) {
		$user = wp_get_current_user();
		$role = $user->roles ? $user->roles[0] : false;
		$url_key = '';

		if(empty($role)) {
			$role = 'guest';
		}

		if($url_specific){
			$url_key = self::get_url_key();
			if(!empty($url_key)){
				$url_key = '.' . $url_key;
			}
		}

		return md5($widget->id) . $url_key . '.' . $role;
	}


	/**
	 * get_url_key function.
	 *
	 * @access public
	 * @static
	 * @return void
	 */
	public static function get_url_key(){
		return md5($_SERVER['REQUEST_URI']);
	}

	/**
	 * Checks to see if cache path exists, if not, creates it and saves the cache data
	 *
	 * @access static
	 * @param string $key
	 * @param string $value
	 * @param int $expiry
	 * @return void
	 */
	public static function set_cache($key, $value, $expiry) {
		$cache = $value;

		if(!self::cache_path_exist()){
			self::create_cache_path();
		}

		file_put_contents(self::get_cache_path() . $key . '.dat', $cache);
	}

	/**
	 * Returns cached data if it exists
	 *
	 * @access static
	 * @param string $key
	 * @return mixed
	 */
	public static function get_cache($key) {
		$config = Wordkeeper_Speed_Config::get_instance()->get_config();

		if(!file_exists(self::get_cache_path() . $key . '.dat')) {
			return false;
		}

		$cache = trim(file_get_contents(self::get_cache_path() . $key . '.dat'));

		if($cache){
			$now = strtotime('now');
			$mtime = filemtime(self::get_cache_path() . $key . '.dat');

			if(($now - $mtime) > $config['wordkeeper']['cache-duration']) {
				return false;
			}

			return $cache;
		}
		return false;
	}

	/**
	 * Deletes all caches. If a filename is supplied, only deletes that cache file
	 *
	 * @access static
	 * @param string $id
	 * @return void
	 */
	public static function delete_cache($id) {
		$filename = md5($id);
		$path = self::get_cache_path() . '*';
		$files = glob($path); //get all file names

		foreach($files as $file){
			if(is_file($file) && strpos($file, $filename) !== false) {
				unlink($file); //delete file
			}
		}
	}

	/**
	 * Purges all cached data
	 *
	 * @access static
	 * @param void
	 * @return void
	 */
	public static function purge_all() {
		$path = self::get_cache_path() . '*';
		$files = glob($path); //get all file names

		if(empty($files) || !$files){
			return false;
		}

		foreach($files as $file){
			if(is_file($file)) {
				unlink($file); //delete file
			}
		}
	}

	/**
	 * Checks if the cache path exists, if not then creates it
	 *
	 * @access static
	 * @param void
	 * @return bool
	 */
	public static function cache_path_exist() {
		if (!is_dir(self::get_cache_path())) {
			return false;
		}
		return true;
	}

	/**
	 * Creates the cache path
	 *
	 * @access static
	 * @param void
	 * @return void
	 */
	public static function create_cache_path() {
		mkdir(self::get_cache_path(), 0775, true);
	}
}
