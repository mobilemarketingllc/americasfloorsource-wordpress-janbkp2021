<?php

/**
 * This class handles the file system based cache management for navigation menus
 *
 * @package:     WordKeeper Speed Menu Cache
 * @author:  WordKeeper
 * @Author URI: http://www.wordkeeper.com
 *
 */
class WordKeeper_Speed_Menu_Cache {

	public static $cache_path = ABSPATH . 'wp-content/cache/partials/menu/';

	/**
	 * Returns the menu cache path
	 *
	 * @access static
	 * @param void
	 * @return mixed
	 */
	public static function get_cache_path(){
		if ( is_multisite() ){
			return ABSPATH . 'wp-content/cache/' . get_current_blog_id() . '/partials/menu/';
		}
		return ABSPATH . 'wp-content/cache/partials/menu/';
	}

	/**
	 * Generates and returns hashed key
	 *
	 * @access static
	 * @param object $args
	 * @return mixed
	 */
	public static function generate_cache_key($args) {
		$user = wp_get_current_user();
		$role = $user->roles ? $user->roles[0] : false;

		if(empty($role)) {
			$role = 'guest';
		}

		return md5($args->menu->slug) . '.' . $role;
	}

	/**
	 * Checks to see if cache path exists, if not, creates it and saves the cache data
	 *
	 * @access static
	 * @param string $key
	 * @param string $value
	 * @param int $expiry
	 * @return void
	 */
	public static function set_cache($key, $value, $expiry) {
		$cache = $value;

		if(!self::cache_path_exist()){
			self::create_cache_path();
		}

		file_put_contents(self::get_cache_path() . $key . '.dat', $cache);
	}

	/**
	 * Returns cached data if it exists
	 *
	 * @access static
	 * @param string $key
	 * @param object $args
	 * @return mixed
	 */
	public static function get_cache($key, $args) {
		$config = Wordkeeper_Speed_Config::get_instance()->get_config();

		if(!file_exists(self::get_cache_path() . $key . '.dat')) {
			return false;
		}

		$cache = trim(file_get_contents(self::get_cache_path() . $key . '.dat'));

		if($cache){
			// Check if cache was expired
			$now = strtotime('now');
			$mtime = filemtime(self::get_cache_path() . $key . '.dat');

			if(($now - $mtime) > $config['wordkeeper']['cache-duration']) {
				return false;
			}

			return $cache;
		}

		return false;
	}

	/**
	 * Purges all cached data
	 *
	 * @access static
	 * @param void
	 * @return void
	 */
	public static function purge_all() {
		$path = self::get_cache_path() . '*';
		$files = glob($path); //get all file names

		if(empty($files) || !$files){
			return false;
		}

		foreach($files as $file){
			if(is_file($file)) {
				unlink($file); //delete file
			}
		}
	}

	/**
	 * Checks if the cache path exists, if not then creates it
	 *
	 * @access static
	 * @param void
	 * @return bool
	 */
	public static function cache_path_exist() {
		if (!is_dir(self::get_cache_path())) {
			return false;
		}
		return true;
	}

	/**
	 * Creates the cache path
	 *
	 * @access static
	 * @param void
	 * @return void
	 */
	public static function create_cache_path() {
		mkdir(self::get_cache_path(), 0775, true);
	}

	/**
	 * Deletes all caches. If a filename is supplied, only deletes that cache file
	 *
	 * @access static
	 * @param string $slug
	 * @return void
	 */
	public static function delete_menu_caches($slug) {
		$filename = md5($slug);
		$path = self::get_cache_path() . '*';
		$files = glob($path); //get all file names

		foreach($files as $file){
			if(is_file($file) && strpos($file, $filename) !== false) {
				unlink($file); //delete file
			}
		}
	}
}
