<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://wordkeeper.com/
 * @since      1.0.0
 *
 * @package    Wordkeeper_Speed
 * @subpackage Wordkeeper_Speed/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wordkeeper_Speed
 * @subpackage Wordkeeper_Speed/admin
 * @author     WordKeeper <wordkeeper@test.com>
 */
class Wordkeeper_Speed_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wordkeeper_Speed_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wordkeeper_Speed_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wordkeeper-speed-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wordkeeper_Speed_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wordkeeper_Speed_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wordkeeper-speed-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * last_checked_now function.
	 *
	 * @access public
	 * @param mixed $transient
	 * @return void
	 */
	function last_checked_now( $transient ) {
		include ABSPATH . WPINC . '/version.php';

		$current = new stdClass;
		$current->updates = array();
		$current->version_checked = $wp_version;
		$current->last_checked = time();

		return $current;
	}

	/**
	 * admin_init function.
	 *
	 * @access public
	 * @return void
	 */
	function admin_init() {
		/*
		* Disable plugin update checks on old WP versions
		*/
		remove_action('load-plugins.php', 'wp_update_plugins');
		remove_action('load-update.php', 'wp_update_plugins');
		remove_action('admin_init', '_maybe_update_plugins');
		remove_action('wp_update_plugins', 'wp_update_plugins');
		wp_clear_scheduled_hook('wp_update_plugins');

		/*
		* Disable plugin update checks on new WP versions
		*/
		remove_action('load-update-core.php', 'wp_update_plugins');
		wp_clear_scheduled_hook('wp_update_plugins');
	}

}
